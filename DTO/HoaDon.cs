﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class HoaDon
    {
        private string maHD;
        private string maKhachHang;
        private string ngayLapDon;
        private string thoiGian;
        private string tongTien;
        private string trangThai;
        public string MaHD { get => maHD; set => maHD = value; }
        public string MaKhachHang { get => maKhachHang; set => maKhachHang = value; }
        public string NgayLapDon { get => ngayLapDon; set => ngayLapDon = value; }
        public string ThoiGian { get => thoiGian; set => thoiGian = value; }
        public string TongTien { get => tongTien; set => tongTien = value; }
        public string TrangThai { get => trangThai; set => trangThai = value; }
        
        public HoaDon()
        {

        }
        public HoaDon(string mahoadon, string makhachhang, string ngaylapdon, string thoigian, string tongtien, string trangthai)
        {
            this.maHD = mahoadon;
            this.MaKhachHang = makhachhang;
            this.NgayLapDon = ngaylapdon;
            this.thoiGian = thoigian;
            this.tongTien = tongtien;
            this.trangThai = trangthai;
        }
    }
}

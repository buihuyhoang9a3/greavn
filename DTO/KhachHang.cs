﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class KhachHang
    {
        private string makhachhang;
        private string tenkhachhang;
        private string sdt;
        private string diachikhachhang;
        private string loaithanhvien;


        public string Makhachhang { get => makhachhang; set => makhachhang = value; }
        public string Tenkhachhang { get => tenkhachhang; set => tenkhachhang = value; }
        public string Sdt { get => sdt; set => sdt = value; }
        public string Diachikhachhang { get => diachikhachhang; set => diachikhachhang = value; }
        public string Loaithanhvien { get => loaithanhvien; set => loaithanhvien = value; }

        public KhachHang()
        {
        }
        public KhachHang(string MaKhachHang, string TenKhachHang, string SDT, string DiaChiKhachHang, string LoaiThanhVien)
        {
            this.makhachhang = MaKhachHang;
            this.tenkhachhang = TenKhachHang;
            this.sdt = SDT;
            this.diachikhachhang = DiaChiKhachHang;
            this.loaithanhvien = LoaiThanhVien;
        }
    }

}

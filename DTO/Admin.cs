﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Admin
    {
        private int id;
        private string username;
        private int level;

        public Admin()
        {
            this.id = 0;
            this.username = "";
            this.level = 1;
        }

        public Admin(int id, string username, int level)
        {
            this.id = id;
            this.username = username;
            this.level = level;
        }

        public Admin(Admin admin)
        {
            this.id = admin.id;
            this.username = admin.username;
            this.level = admin.level;
        }

        public void SetID(int id)
        {
            this.id = id;
        }

        public void SetUsername(string username)
        {
            this.username = username;
        }

        public void SetLevel(int level)
        {
            this.level = level;
        }

        public int GetID()
        {
            return this.id;
        }

        public string GetUsername()
        {
            return this.username;
        }

        public int GetLevel()
        {
            return this.level;
        }
    }
}

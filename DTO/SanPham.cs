﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   public class SanPham
    {
        private string masanspham;
        private string tensanpham;
        private string hangsanpham;
        private string dongsanpham;
        private string giasanpham;
        private int soluong;
        private string maHD;
        private string thanhtien;

        public string Masanspham { get => masanspham; set => masanspham = value; }
        public string Tensanpham { get => tensanpham; set => tensanpham = value; }
        public string Hangsanpham { get => hangsanpham; set => hangsanpham = value; }
        public string Dongsanpham { get => dongsanpham; set => dongsanpham = value; }
        public string Giasanpham { get => giasanpham; set => giasanpham = value; }
        public int Soluong { get => soluong; set => soluong = value; }
        public string MaHD { get => maHD; set => maHD = value; }
        public string Thanhtien { get => thanhtien; set => thanhtien = value; }

        public SanPham(string MaSanPham, string TenSanPham, string HangSanPham, string DongSanPham, string GiaSanPham)
        {
            this.masanspham = MaSanPham;
            this.tensanpham = TenSanPham;
            this.hangsanpham = HangSanPham;
            this.dongsanpham = DongSanPham;
            this.giasanpham = GiaSanPham;
        }
        public SanPham(string MaSanPham, string TenSanPham, string HangSanPham, string DongSanPham, string GiaSanPham, int soluong)
        {
            this.masanspham = MaSanPham;
            this.tensanpham = TenSanPham;
            this.hangsanpham = HangSanPham;
            this.dongsanpham = DongSanPham;
            this.giasanpham = GiaSanPham;
            this.soluong = soluong;
        }

    }
}


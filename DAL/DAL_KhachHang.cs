﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_KhachHang : DBConnect
    {
        DataTable myTable = new DataTable();
        public DataTable Get_KhachHang()
        {
            try
            {
                SqlDataAdapter myDataAdapter = new SqlDataAdapter("select * from KhachHang", myConnection);
                DataTable myTable = new DataTable();

                myDataAdapter.Fill(myTable);
                return myTable;

            }
            catch (Exception err)
            {
                return null;

            }
        }
        public DataTable Get_KhachHang(string sdt)
        {
            try
            {
                SqlDataAdapter myDataAdapter = new SqlDataAdapter("select * from KhachHang where SDT = '" + sdt + "'", myConnection);
                DataTable myTable = new DataTable();

                myDataAdapter.Fill(myTable);
                return myTable;

            }
            catch (Exception err)
            {
                return null;

            }
        }
        public DataTable InsertKH(string tenKH, string SDTKH, string diaChiKH, string loaiKH)
        {

            try
            {
                String query = "insert into dbo.KhachHang(tenKhachHang,SDT,diaChiKhachHang,loaiThanhVien) VALUES (N'" + tenKH + "',N'" + SDTKH + "',N'" + diaChiKH + "',N'" + loaiKH + "')";
                //  Console.WriteLine(query);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, myConnection);

                DataTable myTable = new DataTable();
                //DataView dv = new DataView(myTable);

                myDataAdapter.Fill(myTable);
                Console.WriteLine(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }
        public DataTable UpdateKH(string maID, string tenKH, string SDTKH, string diaChiKH, string loaiKH)
        {

            try
            {
                
                String query = "update dbo.KhachHang set tenKhachHang = '" + tenKH + "',SDT = '" + SDTKH + "',diaChiKhachHang = '" + diaChiKH + "',loaiThanhVien = '" + loaiKH + "' where ID = '" + maID + "'";
                //  Console.WriteLine(query);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, myConnection);

                DataTable myTable = new DataTable();
                myDataAdapter.Fill(myTable);
                Console.WriteLine(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }
        public DataTable DeleteKH(string maID)
        {

            try
            {

                String query = "delete from dbo.KhachHang where ID = '" + maID + "'";
                //  Console.WriteLine(query);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, myConnection);

                DataTable myTable = new DataTable();
                myDataAdapter.Fill(myTable);
                Console.WriteLine(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_SanPham : DBConnect
    {
        public DataTable Get()
        {
			try
			{
                SqlDataAdapter myDataAdapter = new SqlDataAdapter("select * from HangSanPham", myConnection);
                DataTable myTable = new DataTable();
               
                myDataAdapter.Fill(myTable);
                return myTable;

			}
			catch(Exception err)
            {
			    return null;
			}
        }
        public DataTable Get_SanPham()
        {
            try
            {
                SqlDataAdapter myDataAdapter = new SqlDataAdapter("select * from SanPham ", myConnection);
                DataTable myTable = new DataTable();

                myDataAdapter.Fill(myTable);
                return myTable;

            }
            catch (Exception err)
            {
                return null;
            }
        }
        public DataTable Get_maSanPham()
        {
            try
            {
                SqlDataAdapter myDataAdapter = new SqlDataAdapter("select * from dbo.HangSanPham", myConnection);
                DataTable myTable = new DataTable();
                myDataAdapter.Fill(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }
        public DataTable Get_dongSanPham()
        {
            try
            {
                SqlDataAdapter myDataAdapter = new SqlDataAdapter("select * from dbo.DongSanPham", myConnection);
                DataTable myTable = new DataTable();
                myDataAdapter.Fill(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }

        }
        public DataTable Search(string dong, string hang)
        {
            try
            {
                String query = "select * from dbo.SanPham where hangSanPham LIKE '%" + hang + "%' and dongSanPham LIKE '%" + dong + "%'";
                Console.WriteLine(query);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, myConnection);
                
                DataTable myTable = new DataTable();
                //DataView dv = new DataView(myTable);
                //dv.RowFilter = string.Format("hangSanPham LIKE '%{0}%'", dong);

                
                myDataAdapter.Fill(myTable);
                Console.WriteLine(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }
        public DataTable SearchKH(string ten)
        {
            try
            {
                String query = "select * from dbo.KhachHang where SDT LIKE '%" + ten + "%'";
                Console.WriteLine(query);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, myConnection);
                DataTable myTable = new DataTable();
                myDataAdapter.Fill(myTable);
                Console.WriteLine(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }
        DataTable myTable = new DataTable();

        public DAL_SanPham()
        {
            myTable = Get();
          myTable.PrimaryKey = new DataColumn[] { myTable.Columns[1] };
        }
        public DataTable InsertSP(string tenSP, string hangSP, string dongSP, string giaSP)
        {
           
            try
            {
                //Console.WriteLine(tenSP);
                //Console.WriteLine(hangSP);
                //Console.WriteLine(dongSP);
                //Console.WriteLine(giaSP);
                String query = "insert into dbo.SanPham(tenSanPham,hangSanPham,dongSanPham,giaSanPham) VALUES (N'"+tenSP+ "','" + hangSP + "','" + dongSP + "','" + giaSP + "')";
              //  Console.WriteLine(query);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, myConnection);

                DataTable myTable = new DataTable();
                //DataView dv = new DataView(myTable);

                myDataAdapter.Fill(myTable);
                Console.WriteLine(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }
        public DataTable UpdateSP(string maSP,string tenSP, string hangSP, string dongSP, string giaSP)
        {

            try
            {
                //Console.WriteLine(tenSP);
                //Console.WriteLine(hangSP);
                //Console.WriteLine(dongSP);
                //Console.WriteLine(giaSP);
                String query = "update dbo.SanPham set tenSanPham = '" + tenSP + "',hangSanPham = '" + hangSP + "',dongSanPham = '" + dongSP + "',giaSanPham = '" + giaSP +"' where maSanPham = '" + maSP + "'";
                //  Console.WriteLine(query);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, myConnection);

                DataTable myTable = new DataTable();
                myDataAdapter.Fill(myTable);
                Console.WriteLine(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }
        public DataTable DeleteSP(string maSP)
        {

            try
            {

                String query = "delete from dbo.SanPham where maSanPham = '" + maSP + "'";
                //  Console.WriteLine(query);
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(query, myConnection);

                DataTable myTable = new DataTable();
                myDataAdapter.Fill(myTable);
                Console.WriteLine(myTable);
                return myTable;
            }
            catch
            {
                return null;
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
    public class DAL_HoaDoncs:DBConnect
    {
        public string LayLastID()
        {
            if(myConnection.State == ConnectionState.Closed)
            {
                myConnection.Open();
            }    
            try
            {
                SqlDataAdapter myDataAdapter = new SqlDataAdapter("select max(maHD) as maHD from HoaDon ", myConnection);
                DataTable myTable = new DataTable();

                myDataAdapter.Fill(myTable);
                return myTable.Rows[0]["maHD"].ToString();

            }
            catch (Exception err)
            {
                return null;
            }
        }
        public bool InsertHoaDon(DTO.HoaDon hd)
        {
            if (myConnection.State == ConnectionState.Closed)
            {
                myConnection.Open();
            }
            Boolean check = false;
            try
            {
                string query = "insert into HoaDon(maKhachHang,ngayLapDon,thoiGian,tongTien,trangThai) values('"+hd.MaKhachHang+"','"+hd.NgayLapDon+"','"+hd.ThoiGian+"','"+hd.TongTien+"',1)";
                SqlCommand sc = new SqlCommand(query, myConnection);
                sc.ExecuteNonQuery();
                check = true;
                

            }
            catch (Exception err)
            {
                return false;
            }
            return check;
        }
        public bool InsertChiTietHoaDon(DTO.SanPham sp, string maHD)
        {
            if (myConnection.State == ConnectionState.Closed)
            {
                myConnection.Open();
            }
            Boolean check = false;
            try
            {
                
                double thanhtien = sp.Soluong * int.Parse(sp.Giasanpham);
                string query = "insert into ChiTietHoaDon(maHD,soLuong,thanhTien,trangThai,maSP) values('"+maHD+"','"+sp.Soluong+"','"+thanhtien+"',1,'"+sp.Masanspham+"')";
                SqlCommand sc = new SqlCommand(query, myConnection);
                sc.ExecuteNonQuery();
                check = true;


            }
            catch (Exception err)
            {
                return false;
            }
            return check;
        }
        public DataSet LayThongTinHoaDon(string mahd)
        {
            try
            {
                SqlDataAdapter myDataAdapter = new SqlDataAdapter("select * from ChiTietHoaDon,HoaDon,KhachHang,SanPham where ChiTietHoaDon.maHD=HoaDon.maHD and HoaDon.maKhachHang=KhachHang.ID and ChiTietHoaDon.maSP=SanPham.maSanPham and HoaDon.maHD = '"+mahd+"'", myConnection);
                DataSet myTable = new DataSet();

                myDataAdapter.Fill(myTable);
                return myTable;

            }
            catch (Exception err)
            {
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DTO;

namespace DAL
{
    public  class DAL_Admin : DBConnect
    {
        public Exception Create(String userName, String password, string level)
        {
            try
            {
                myConnection.Open();
                String query = "insert into AdminGear(AdminName, AdminPassword, AdminLevel) values ('" + userName + "', '" + password + "', '" + level + "');";
                SqlCommand command = new SqlCommand(query, myConnection);
                command.ExecuteNonQuery();
                myConnection.Close();
            }
            catch (Exception err)
            {
                return err;
            }
            return null;
        }

        /* Function này trả về Level của admin (1 là admin, 2 là nhân viên) */
        public DTO.Admin Verify(String userName, String password)
        {
            myConnection.Open();
            String query = "select ID, AdminName, AdminLevel from AdminGear where AdminName = '" + userName + "' and AdminPassword = '" + password + "';";
            SqlCommand command = new SqlCommand(query, myConnection);
            SqlDataReader reader = command.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    int level = reader.GetInt32(reader.GetOrdinal("AdminLevel"));
                    int id = reader.GetInt32(reader.GetOrdinal("ID"));
                    string username = reader.GetString(reader.GetOrdinal("AdminName"));
                    Admin adminReturn = new Admin(id, username, level);
                    myConnection.Close();
                    return adminReturn;
                }
            }
            myConnection.Close();
            Admin adminFail = new Admin();
            return adminFail;
        }
    }
}

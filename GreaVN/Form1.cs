﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BUS;
using DTO;
using System.Windows.Forms.VisualStyles;

namespace GreaVN
{
    public partial class Form1 : Form
    {
        BUS_Admin admin = new BUS_Admin();
        Admin adminData = new Admin();
        private string username;
        private string password;
        private DTO.KhachHang dtoKhachHang = new DTO.KhachHang();
        private DTO.Admin dtoAdmin = new DTO.Admin();
        public Form1()
        {
            InitializeComponent();
            this.CenterToScreen();
            this.username = "";
            this.password = "";
        }



        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            //this.username = "admin";
            //this.password = "12345";
            if (this.username != "" && this.password != "")
            {
                adminData = admin.Verify(this.username, this.password);
                if (adminData.GetID() != 0)
                {
                   
                    Form2 f = new Form2();
                    this.Hide();
                    f.ShowDialog();

                }
                else
                {
                    DialogResult h = MessageBox.Show
                    ("Tài khoảng không đúng", "Error", MessageBoxButtons.OKCancel);
                }
            }
            else
            {
                DialogResult h = MessageBox.Show
                ("Không được để trống Username hoặc Password", "Error", MessageBoxButtons.OKCancel);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult h = MessageBox.Show
              ("Bạn có chắc muốn thoát không?", "Error", MessageBoxButtons.OKCancel);
            if (h == DialogResult.OK)
                Application.Exit();

        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có thật sự muốn thoát?", "Thông báo!", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk) != System.Windows.Forms.DialogResult.OK)
            {
                e.Cancel = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtDangNhap.Text = this.username;
            txtMatKhau.Text = this.password;
        }

        private void txtDangNhap_TextChanged(object sender, EventArgs e)
        {
            this.username = txtDangNhap.Text;
        }

        private void txtMatKhau_TextChanged(object sender, EventArgs e)
        {
            this.password = txtMatKhau.Text;
        }
    }
}

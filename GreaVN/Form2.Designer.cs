﻿namespace GreaVN
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tTroGiup = new System.Windows.Forms.TabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.btnDangXuat = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tQuanLy = new System.Windows.Forms.TabPage();
            this.btnThoatQL = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnKhachHang = new System.Windows.Forms.Button();
            this.btnSanPham = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.sanPham1 = new GreaVN.SanPham();
            this.khachHang2 = new GreaVN.KhachHang();
            this.tKhachHang = new System.Windows.Forms.TabPage();
            this.dgvKH = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenKhachHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diaChiKhachHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loaiThanhVien = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnThoatKH = new System.Windows.Forms.Button();
            this.lKhuVucKH = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnTimTenKH = new System.Windows.Forms.Button();
            this.lTimTenKH = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lLTVKH = new System.Windows.Forms.Label();
            this.lSDTKH = new System.Windows.Forms.Label();
            this.lHoTenKH = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lalala = new System.Windows.Forms.Label();
            this.tSanPham = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.numSoluong = new System.Windows.Forms.NumericUpDown();
            this.labSoLuong = new System.Windows.Forms.Label();
            this.btnThoatSP = new System.Windows.Forms.Button();
            this.btnTimKiem = new System.Windows.Forms.Button();
            this.btnThem = new System.Windows.Forms.Button();
            this.lDong = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cboDong = new System.Windows.Forms.ComboBox();
            this.cboHang = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lDonGia = new System.Windows.Forms.Label();
            this.lHang = new System.Windows.Forms.Label();
            this.lTenSanPham = new System.Windows.Forms.Label();
            this.lMaSanPham = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvSanPham = new System.Windows.Forms.DataGridView();
            this.maSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dongSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hangSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.giaSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tThanhToan = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.labHoTenKH = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLamMoi = new System.Windows.Forms.Button();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnThanhToan = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.cboPhanTram = new System.Windows.Forms.ComboBox();
            this.lThoiGian = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lvHoaDon = new System.Windows.Forms.ListView();
            this.STT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvMSP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvTSP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvHang = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvGia = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvSoLuong = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvThanhTien = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtTongTien = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tTroGiup.SuspendLayout();
            this.tQuanLy.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tKhachHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKH)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tSanPham.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSoluong)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSanPham)).BeginInit();
            this.tThanhToan.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Manager.png");
            this.imageList1.Images.SetKeyName(1, "bills.jpg");
            this.imageList1.Images.SetKeyName(2, "help.png");
            this.imageList1.Images.SetKeyName(3, "laptop.png");
            this.imageList1.Images.SetKeyName(4, "members.jpg");
            // 
            // tTroGiup
            // 
            this.tTroGiup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tTroGiup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tTroGiup.Controls.Add(this.label28);
            this.tTroGiup.Controls.Add(this.label27);
            this.tTroGiup.Controls.Add(this.label26);
            this.tTroGiup.Controls.Add(this.label25);
            this.tTroGiup.Controls.Add(this.btnDangXuat);
            this.tTroGiup.Controls.Add(this.label24);
            this.tTroGiup.Controls.Add(this.label23);
            this.tTroGiup.Controls.Add(this.label21);
            this.tTroGiup.ImageIndex = 2;
            this.tTroGiup.Location = new System.Drawing.Point(4, 124);
            this.tTroGiup.Name = "tTroGiup";
            this.tTroGiup.Padding = new System.Windows.Forms.Padding(3);
            this.tTroGiup.Size = new System.Drawing.Size(1032, 531);
            this.tTroGiup.TabIndex = 4;
            this.tTroGiup.Text = "Trợ giúp";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(129, 310);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(723, 26);
            this.label28.TabIndex = 8;
            this.label28.Text = "Có thắc mắc, khó khăn hoặc không biết xin hãy hỏi, xin đừng im lặng.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(128, 250);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(759, 26);
            this.label27.TabIndex = 7;
            this.label27.Text = "Fanpage của quán: Hoành Thánh Công Nghệ (fb.com/khanhkhanghoang)";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(187, 189);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(605, 26);
            this.label26.TabIndex = 6;
            this.label26.Text = "SĐT quản lý: 113 (A. Tám) , 114 (A. Khét), 115 (A. Ngủm).";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(103, 130);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(784, 26);
            this.label25.TabIndex = 5;
            this.label25.Text = "Chấm công khi bắt đầu làm việc và khi tan làm để bảo vệ quyền lợi của bạn.";
            // 
            // btnDangXuat
            // 
            this.btnDangXuat.ForeColor = System.Drawing.Color.Red;
            this.btnDangXuat.Location = new System.Drawing.Point(477, 363);
            this.btnDangXuat.Name = "btnDangXuat";
            this.btnDangXuat.Size = new System.Drawing.Size(130, 24);
            this.btnDangXuat.TabIndex = 4;
            this.btnDangXuat.Text = "Click";
            this.btnDangXuat.UseVisualStyleBackColor = true;
            this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click_1);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(344, 360);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(117, 27);
            this.label24.TabIndex = 3;
            this.label24.Text = "Đăng xuất:";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(73, 66);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(884, 26);
            this.label23.TabIndex = 1;
            this.label23.Text = "Mọi ý kiến, góp ý xin nhắn tin đến SĐT, Zalo của quản lý (Nội dung sẽ được bảo mậ" +
    "t).";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(186, 14);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(666, 34);
            this.label21.TabIndex = 0;
            this.label21.Text = "Hãy luôn lịch sự, thân thiện, hòa đồng trong công việc.";
            // 
            // tQuanLy
            // 
            this.tQuanLy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tQuanLy.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tQuanLy.Controls.Add(this.btnThoatQL);
            this.tQuanLy.Controls.Add(this.panel8);
            this.tQuanLy.Controls.Add(this.sanPham1);
            this.tQuanLy.Controls.Add(this.khachHang2);
            this.tQuanLy.ImageIndex = 0;
            this.tQuanLy.Location = new System.Drawing.Point(4, 124);
            this.tQuanLy.Name = "tQuanLy";
            this.tQuanLy.Padding = new System.Windows.Forms.Padding(3);
            this.tQuanLy.Size = new System.Drawing.Size(1032, 531);
            this.tQuanLy.TabIndex = 3;
            this.tQuanLy.Text = "Quản lý";
            // 
            // btnThoatQL
            // 
            this.btnThoatQL.Location = new System.Drawing.Point(927, 15);
            this.btnThoatQL.Name = "btnThoatQL";
            this.btnThoatQL.Size = new System.Drawing.Size(81, 35);
            this.btnThoatQL.TabIndex = 33;
            this.btnThoatQL.Text = "Thoát";
            this.btnThoatQL.UseVisualStyleBackColor = true;
            this.btnThoatQL.Click += new System.EventHandler(this.btnThoatQL_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnKhachHang);
            this.panel8.Controls.Add(this.btnSanPham);
            this.panel8.Controls.Add(this.label12);
            this.panel8.Location = new System.Drawing.Point(510, 6);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(365, 52);
            this.panel8.TabIndex = 2;
            // 
            // btnKhachHang
            // 
            this.btnKhachHang.Location = new System.Drawing.Point(236, 9);
            this.btnKhachHang.Name = "btnKhachHang";
            this.btnKhachHang.Size = new System.Drawing.Size(113, 33);
            this.btnKhachHang.TabIndex = 2;
            this.btnKhachHang.Text = "Khách Hàng";
            this.btnKhachHang.UseVisualStyleBackColor = true;
            this.btnKhachHang.Click += new System.EventHandler(this.btnKhachHang_Click);
            // 
            // btnSanPham
            // 
            this.btnSanPham.Location = new System.Drawing.Point(94, 9);
            this.btnSanPham.Name = "btnSanPham";
            this.btnSanPham.Size = new System.Drawing.Size(117, 31);
            this.btnSanPham.TabIndex = 1;
            this.btnSanPham.Text = "Sản phẩm";
            this.btnSanPham.UseVisualStyleBackColor = true;
            this.btnSanPham.Click += new System.EventHandler(this.btnSanPham_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "Chọn bảng:";
            // 
            // sanPham1
            // 
            this.sanPham1.Location = new System.Drawing.Point(6, 52);
            this.sanPham1.Name = "sanPham1";
            this.sanPham1.Size = new System.Drawing.Size(1002, 370);
            this.sanPham1.TabIndex = 32;
            // 
            // khachHang2
            // 
            this.khachHang2.Location = new System.Drawing.Point(6, 52);
            this.khachHang2.Name = "khachHang2";
            this.khachHang2.Size = new System.Drawing.Size(1028, 352);
            this.khachHang2.TabIndex = 31;
            this.khachHang2.Load += new System.EventHandler(this.khachHang2_Load);
            // 
            // tKhachHang
            // 
            this.tKhachHang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tKhachHang.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tKhachHang.Controls.Add(this.dgvKH);
            this.tKhachHang.Controls.Add(this.panel5);
            this.tKhachHang.ImageIndex = 4;
            this.tKhachHang.Location = new System.Drawing.Point(4, 124);
            this.tKhachHang.Name = "tKhachHang";
            this.tKhachHang.Padding = new System.Windows.Forms.Padding(3);
            this.tKhachHang.Size = new System.Drawing.Size(1032, 531);
            this.tKhachHang.TabIndex = 2;
            this.tKhachHang.Text = "Khách hàng";
            this.tKhachHang.Click += new System.EventHandler(this.tKhachHang_Click);
            // 
            // dgvKH
            // 
            this.dgvKH.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvKH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.tenKhachHang,
            this.SDT,
            this.diaChiKhachHang,
            this.loaiThanhVien});
            this.dgvKH.Location = new System.Drawing.Point(40, 160);
            this.dgvKH.Name = "dgvKH";
            this.dgvKH.RowHeadersWidth = 51;
            this.dgvKH.RowTemplate.Height = 24;
            this.dgvKH.Size = new System.Drawing.Size(962, 301);
            this.dgvKH.TabIndex = 18;
            this.dgvKH.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKH_CellContentClick);
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            // 
            // tenKhachHang
            // 
            this.tenKhachHang.DataPropertyName = "tenKhachHang";
            this.tenKhachHang.HeaderText = "Tên khách hàng";
            this.tenKhachHang.MinimumWidth = 6;
            this.tenKhachHang.Name = "tenKhachHang";
            // 
            // SDT
            // 
            this.SDT.DataPropertyName = "SDT";
            this.SDT.HeaderText = "Số điện thoại";
            this.SDT.MinimumWidth = 6;
            this.SDT.Name = "SDT";
            // 
            // diaChiKhachHang
            // 
            this.diaChiKhachHang.DataPropertyName = "diaChiKhachHang";
            this.diaChiKhachHang.HeaderText = "Địa chỉ ";
            this.diaChiKhachHang.MinimumWidth = 6;
            this.diaChiKhachHang.Name = "diaChiKhachHang";
            // 
            // loaiThanhVien
            // 
            this.loaiThanhVien.DataPropertyName = "loaiThanhVien";
            this.loaiThanhVien.HeaderText = "Loại Thành Viên";
            this.loaiThanhVien.MinimumWidth = 6;
            this.loaiThanhVien.Name = "loaiThanhVien";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnThoatKH);
            this.panel5.Controls.Add(this.lKhuVucKH);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.lLTVKH);
            this.panel5.Controls.Add(this.lSDTKH);
            this.panel5.Controls.Add(this.lHoTenKH);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.lalala);
            this.panel5.Location = new System.Drawing.Point(22, 31);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1000, 123);
            this.panel5.TabIndex = 16;
            // 
            // btnThoatKH
            // 
            this.btnThoatKH.Location = new System.Drawing.Point(883, 10);
            this.btnThoatKH.Name = "btnThoatKH";
            this.btnThoatKH.Size = new System.Drawing.Size(97, 35);
            this.btnThoatKH.TabIndex = 18;
            this.btnThoatKH.Text = "Thoát";
            this.btnThoatKH.UseVisualStyleBackColor = true;
            this.btnThoatKH.Click += new System.EventHandler(this.btnThoatKH_Click);
            // 
            // lKhuVucKH
            // 
            this.lKhuVucKH.AutoSize = true;
            this.lKhuVucKH.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lKhuVucKH.Location = new System.Drawing.Point(418, 11);
            this.lKhuVucKH.Name = "lKhuVucKH";
            this.lKhuVucKH.Size = new System.Drawing.Size(92, 19);
            this.lKhuVucKH.TabIndex = 11;
            this.lKhuVucKH.Text = "lKhuVucKH";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnTimTenKH);
            this.panel7.Controls.Add(this.lTimTenKH);
            this.panel7.Controls.Add(this.label22);
            this.panel7.Location = new System.Drawing.Point(422, 72);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(558, 39);
            this.panel7.TabIndex = 17;
            // 
            // btnTimTenKH
            // 
            this.btnTimTenKH.Location = new System.Drawing.Point(445, 6);
            this.btnTimTenKH.Name = "btnTimTenKH";
            this.btnTimTenKH.Size = new System.Drawing.Size(95, 26);
            this.btnTimTenKH.TabIndex = 14;
            this.btnTimTenKH.Text = "Tìm";
            this.btnTimTenKH.UseVisualStyleBackColor = true;
            this.btnTimTenKH.Click += new System.EventHandler(this.btnTimTenKH_Click);
            // 
            // lTimTenKH
            // 
            this.lTimTenKH.Location = new System.Drawing.Point(186, 8);
            this.lTimTenKH.Name = "lTimTenKH";
            this.lTimTenKH.Size = new System.Drawing.Size(244, 22);
            this.lTimTenKH.TabIndex = 13;
            this.lTimTenKH.TextChanged += new System.EventHandler(this.lTimTenKH_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(31, 13);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(137, 19);
            this.label22.TabIndex = 12;
            this.label22.Text = "Tìm theo SĐT KH:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(342, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(70, 19);
            this.label13.TabIndex = 10;
            this.label13.Text = "Khu vực:";
            // 
            // lLTVKH
            // 
            this.lLTVKH.AutoSize = true;
            this.lLTVKH.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lLTVKH.Location = new System.Drawing.Point(149, 92);
            this.lLTVKH.Name = "lLTVKH";
            this.lLTVKH.Size = new System.Drawing.Size(66, 19);
            this.lLTVKH.TabIndex = 7;
            this.lLTVKH.Text = "lLTVKH";
            // 
            // lSDTKH
            // 
            this.lSDTKH.AutoSize = true;
            this.lSDTKH.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lSDTKH.Location = new System.Drawing.Point(147, 51);
            this.lSDTKH.Name = "lSDTKH";
            this.lSDTKH.Size = new System.Drawing.Size(68, 19);
            this.lSDTKH.TabIndex = 6;
            this.lSDTKH.Text = "lSDTKH";
            // 
            // lHoTenKH
            // 
            this.lHoTenKH.AutoSize = true;
            this.lHoTenKH.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lHoTenKH.Location = new System.Drawing.Point(149, 11);
            this.lHoTenKH.Name = "lHoTenKH";
            this.lHoTenKH.Size = new System.Drawing.Size(83, 19);
            this.lHoTenKH.TabIndex = 5;
            this.lHoTenKH.Text = "lHoTenKH";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(26, 92);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(117, 19);
            this.label19.TabIndex = 2;
            this.label19.Text = "Loại thành viên:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(26, 51);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 19);
            this.label20.TabIndex = 1;
            this.label20.Text = "SĐT:";
            // 
            // lalala
            // 
            this.lalala.AutoSize = true;
            this.lalala.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lalala.Location = new System.Drawing.Point(26, 11);
            this.lalala.Name = "lalala";
            this.lalala.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lalala.Size = new System.Drawing.Size(60, 19);
            this.lalala.TabIndex = 0;
            this.lalala.Text = "Họ tên:";
            // 
            // tSanPham
            // 
            this.tSanPham.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tSanPham.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tSanPham.Controls.Add(this.panel4);
            this.tSanPham.Controls.Add(this.dgvSanPham);
            this.tSanPham.ImageIndex = 3;
            this.tSanPham.Location = new System.Drawing.Point(4, 124);
            this.tSanPham.Name = "tSanPham";
            this.tSanPham.Padding = new System.Windows.Forms.Padding(3);
            this.tSanPham.Size = new System.Drawing.Size(1032, 531);
            this.tSanPham.TabIndex = 1;
            this.tSanPham.Text = "Sản phẩm";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.numSoluong);
            this.panel4.Controls.Add(this.labSoLuong);
            this.panel4.Controls.Add(this.btnThoatSP);
            this.panel4.Controls.Add(this.btnTimKiem);
            this.panel4.Controls.Add(this.btnThem);
            this.panel4.Controls.Add(this.lDong);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.lDonGia);
            this.panel4.Controls.Add(this.lHang);
            this.panel4.Controls.Add(this.lTenSanPham);
            this.panel4.Controls.Add(this.lMaSanPham);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Location = new System.Drawing.Point(23, 6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(999, 231);
            this.panel4.TabIndex = 15;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // numSoluong
            // 
            this.numSoluong.Location = new System.Drawing.Point(118, 195);
            this.numSoluong.Name = "numSoluong";
            this.numSoluong.Size = new System.Drawing.Size(55, 22);
            this.numSoluong.TabIndex = 21;
            // 
            // labSoLuong
            // 
            this.labSoLuong.AutoSize = true;
            this.labSoLuong.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labSoLuong.Location = new System.Drawing.Point(9, 196);
            this.labSoLuong.Name = "labSoLuong";
            this.labSoLuong.Size = new System.Drawing.Size(75, 19);
            this.labSoLuong.TabIndex = 20;
            this.labSoLuong.Text = "Số lượng:";
            // 
            // btnThoatSP
            // 
            this.btnThoatSP.Location = new System.Drawing.Point(894, 11);
            this.btnThoatSP.Name = "btnThoatSP";
            this.btnThoatSP.Size = new System.Drawing.Size(86, 36);
            this.btnThoatSP.TabIndex = 19;
            this.btnThoatSP.Text = "Thoát";
            this.btnThoatSP.UseVisualStyleBackColor = true;
            this.btnThoatSP.Click += new System.EventHandler(this.btnThoatSP_Click);
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Location = new System.Drawing.Point(815, 162);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(141, 33);
            this.btnTimKiem.TabIndex = 18;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.UseVisualStyleBackColor = true;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // btnThem
            // 
            this.btnThem.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThem.Location = new System.Drawing.Point(815, 118);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(141, 33);
            this.btnThem.TabIndex = 17;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lDong
            // 
            this.lDong.AutoSize = true;
            this.lDong.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDong.Location = new System.Drawing.Point(117, 132);
            this.lDong.Name = "lDong";
            this.lDong.Size = new System.Drawing.Size(0, 19);
            this.lDong.TabIndex = 11;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.cboDong);
            this.panel6.Controls.Add(this.cboHang);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Location = new System.Drawing.Point(291, 160);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(503, 39);
            this.panel6.TabIndex = 16;
            // 
            // cboDong
            // 
            this.cboDong.FormattingEnabled = true;
            this.cboDong.Location = new System.Drawing.Point(345, 8);
            this.cboDong.Name = "cboDong";
            this.cboDong.Size = new System.Drawing.Size(152, 24);
            this.cboDong.TabIndex = 14;
            this.cboDong.SelectedIndexChanged += new System.EventHandler(this.cboDong_SelectedIndexChanged);
            // 
            // cboHang
            // 
            this.cboHang.FormattingEnabled = true;
            this.cboHang.Location = new System.Drawing.Point(164, 8);
            this.cboHang.Name = "cboHang";
            this.cboHang.Size = new System.Drawing.Size(152, 24);
            this.cboHang.TabIndex = 13;
            this.cboHang.SelectedIndexChanged += new System.EventHandler(this.cboHang_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(40, 11);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 19);
            this.label11.TabIndex = 12;
            this.label11.Text = "Lọc sản phẩm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 132);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 19);
            this.label10.TabIndex = 10;
            this.label10.Text = "Dòng:";
            // 
            // lDonGia
            // 
            this.lDonGia.AutoSize = true;
            this.lDonGia.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lDonGia.Location = new System.Drawing.Point(117, 166);
            this.lDonGia.Name = "lDonGia";
            this.lDonGia.Size = new System.Drawing.Size(0, 19);
            this.lDonGia.TabIndex = 8;
            // 
            // lHang
            // 
            this.lHang.AutoSize = true;
            this.lHang.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lHang.Location = new System.Drawing.Point(117, 92);
            this.lHang.Name = "lHang";
            this.lHang.Size = new System.Drawing.Size(0, 19);
            this.lHang.TabIndex = 7;
            // 
            // lTenSanPham
            // 
            this.lTenSanPham.AutoSize = true;
            this.lTenSanPham.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lTenSanPham.Location = new System.Drawing.Point(117, 51);
            this.lTenSanPham.Name = "lTenSanPham";
            this.lTenSanPham.Size = new System.Drawing.Size(0, 19);
            this.lTenSanPham.TabIndex = 6;
            // 
            // lMaSanPham
            // 
            this.lMaSanPham.AutoSize = true;
            this.lMaSanPham.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lMaSanPham.Location = new System.Drawing.Point(114, 11);
            this.lMaSanPham.Name = "lMaSanPham";
            this.lMaSanPham.Size = new System.Drawing.Size(0, 19);
            this.lMaSanPham.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 19);
            this.label9.TabIndex = 3;
            this.label9.Text = "Đơn giá:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 92);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 19);
            this.label8.TabIndex = 2;
            this.label8.Text = "Hãng:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 19);
            this.label7.TabIndex = 1;
            this.label7.Text = "Tên sản phẩm:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 19);
            this.label6.TabIndex = 0;
            this.label6.Text = "Mã sản phẩm:";
            // 
            // dgvSanPham
            // 
            this.dgvSanPham.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvSanPham.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSanPham.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSanPham.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.maSanPham,
            this.tenSanPham,
            this.dongSanPham,
            this.hangSanPham,
            this.giaSanPham});
            this.dgvSanPham.Location = new System.Drawing.Point(23, 243);
            this.dgvSanPham.Name = "dgvSanPham";
            this.dgvSanPham.ReadOnly = true;
            this.dgvSanPham.RowHeadersWidth = 51;
            this.dgvSanPham.RowTemplate.Height = 24;
            this.dgvSanPham.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSanPham.Size = new System.Drawing.Size(999, 245);
            this.dgvSanPham.TabIndex = 9;
            this.dgvSanPham.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSanPham_CellClick);
            this.dgvSanPham.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSanPham_CellContentClick);
            // 
            // maSanPham
            // 
            this.maSanPham.DataPropertyName = "maSanPham";
            this.maSanPham.HeaderText = "Mã sản phẩm";
            this.maSanPham.MinimumWidth = 6;
            this.maSanPham.Name = "maSanPham";
            this.maSanPham.ReadOnly = true;
            // 
            // tenSanPham
            // 
            this.tenSanPham.DataPropertyName = "tenSanPham";
            this.tenSanPham.HeaderText = "Tên Sản Phẩm";
            this.tenSanPham.MinimumWidth = 6;
            this.tenSanPham.Name = "tenSanPham";
            this.tenSanPham.ReadOnly = true;
            // 
            // dongSanPham
            // 
            this.dongSanPham.DataPropertyName = "dongSanPham";
            this.dongSanPham.HeaderText = "Dòng Sản Phẩm";
            this.dongSanPham.MinimumWidth = 6;
            this.dongSanPham.Name = "dongSanPham";
            this.dongSanPham.ReadOnly = true;
            // 
            // hangSanPham
            // 
            this.hangSanPham.DataPropertyName = "hangSanPham";
            this.hangSanPham.HeaderText = "Hãng Sản Phẩm";
            this.hangSanPham.MinimumWidth = 6;
            this.hangSanPham.Name = "hangSanPham";
            this.hangSanPham.ReadOnly = true;
            // 
            // giaSanPham
            // 
            this.giaSanPham.DataPropertyName = "giaSanPham";
            this.giaSanPham.HeaderText = "Giá Sản Phẩm";
            this.giaSanPham.MinimumWidth = 6;
            this.giaSanPham.Name = "giaSanPham";
            this.giaSanPham.ReadOnly = true;
            // 
            // tThanhToan
            // 
            this.tThanhToan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tThanhToan.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tThanhToan.Controls.Add(this.groupBox1);
            this.tThanhToan.Font = new System.Drawing.Font("Arial Narrow", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tThanhToan.ImageIndex = 1;
            this.tThanhToan.Location = new System.Drawing.Point(4, 124);
            this.tThanhToan.Name = "tThanhToan";
            this.tThanhToan.Padding = new System.Windows.Forms.Padding(3);
            this.tThanhToan.Size = new System.Drawing.Size(1032, 531);
            this.tThanhToan.TabIndex = 0;
            this.tThanhToan.Text = "Thanh toán";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnLamMoi);
            this.groupBox1.Controls.Add(this.btnThoat);
            this.groupBox1.Controls.Add(this.btnThanhToan);
            this.groupBox1.Controls.Add(this.btnXoa);
            this.groupBox1.Controls.Add(this.cboPhanTram);
            this.groupBox1.Controls.Add(this.lThoiGian);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.lvHoaDon);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1016, 455);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Hóa đơn";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(768, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(224, 29);
            this.label4.TabIndex = 11;
            this.label4.Text = "Quản trị viên: Admin";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.labHoTenKH);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtSDT);
            this.groupBox3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(761, 143);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(260, 124);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Khách hàng";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 22);
            this.label3.TabIndex = 8;
            this.label3.Text = "SDT:";
            // 
            // labHoTenKH
            // 
            this.labHoTenKH.AutoSize = true;
            this.labHoTenKH.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labHoTenKH.Location = new System.Drawing.Point(121, 89);
            this.labHoTenKH.Name = "labHoTenKH";
            this.labHoTenKH.Size = new System.Drawing.Size(0, 22);
            this.labHoTenKH.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 22);
            this.label5.TabIndex = 6;
            this.label5.Text = "Họ tên:";
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(86, 44);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(162, 30);
            this.txtSDT.TabIndex = 0;
            this.txtSDT.TextChanged += new System.EventHandler(this.txtSDT_TextChanged);
            this.txtSDT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSDT_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(367, 363);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 23);
            this.label1.TabIndex = 10;
            this.label1.Text = "%";
            // 
            // btnLamMoi
            // 
            this.btnLamMoi.Location = new System.Drawing.Point(638, 50);
            this.btnLamMoi.Name = "btnLamMoi";
            this.btnLamMoi.Size = new System.Drawing.Size(106, 36);
            this.btnLamMoi.TabIndex = 9;
            this.btnLamMoi.Text = "Làm mới";
            this.btnLamMoi.UseVisualStyleBackColor = true;
            this.btnLamMoi.Click += new System.EventHandler(this.btnLamMoi_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(908, 360);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(90, 37);
            this.btnThoat.TabIndex = 8;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThanhToan.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btnThanhToan.Location = new System.Drawing.Point(456, 360);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(278, 34);
            this.btnThanhToan.TabIndex = 4;
            this.btnThanhToan.Text = "Thanh toán";
            this.btnThanhToan.UseVisualStyleBackColor = true;
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click_1);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(638, 92);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(106, 34);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // cboPhanTram
            // 
            this.cboPhanTram.FormattingEnabled = true;
            this.cboPhanTram.Items.AddRange(new object[] {
            "0",
            "10",
            "20",
            "30"});
            this.cboPhanTram.Location = new System.Drawing.Point(277, 360);
            this.cboPhanTram.Name = "cboPhanTram";
            this.cboPhanTram.Size = new System.Drawing.Size(84, 31);
            this.cboPhanTram.TabIndex = 2;
            this.cboPhanTram.Text = "0";
            this.cboPhanTram.SelectedIndexChanged += new System.EventHandler(this.cboLoaiKH_SelectedIndexChanged);
            this.cboPhanTram.TextChanged += new System.EventHandler(this.cboLoaiKH_TextChanged);
            // 
            // lThoiGian
            // 
            this.lThoiGian.AutoSize = true;
            this.lThoiGian.Location = new System.Drawing.Point(904, 26);
            this.lThoiGian.Name = "lThoiGian";
            this.lThoiGian.Size = new System.Drawing.Size(94, 23);
            this.lThoiGian.TabIndex = 0;
            this.lThoiGian.Text = "lThoiGian";
            this.lThoiGian.Click += new System.EventHandler(this.lThoiGian_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(768, 20);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(118, 30);
            this.dateTimePicker1.TabIndex = 0;
            this.dateTimePicker1.TabStop = false;
            // 
            // lvHoaDon
            // 
            this.lvHoaDon.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.STT,
            this.lvMSP,
            this.lvTSP,
            this.lvHang,
            this.lvGia,
            this.lvSoLuong,
            this.lvThanhTien});
            this.lvHoaDon.FullRowSelect = true;
            this.lvHoaDon.HideSelection = false;
            this.lvHoaDon.Location = new System.Drawing.Point(13, 30);
            this.lvHoaDon.Name = "lvHoaDon";
            this.lvHoaDon.Size = new System.Drawing.Size(619, 297);
            this.lvHoaDon.TabIndex = 0;
            this.lvHoaDon.UseCompatibleStateImageBehavior = false;
            this.lvHoaDon.View = System.Windows.Forms.View.Details;
            // 
            // STT
            // 
            this.STT.Text = "STT";
            // 
            // lvMSP
            // 
            this.lvMSP.Text = "Mã sản phẩm";
            this.lvMSP.Width = 137;
            // 
            // lvTSP
            // 
            this.lvTSP.Text = "Tên sản phẩm";
            this.lvTSP.Width = 137;
            // 
            // lvHang
            // 
            this.lvHang.Text = "Hãng";
            this.lvHang.Width = 63;
            // 
            // lvGia
            // 
            this.lvGia.Text = "Đơn giá";
            this.lvGia.Width = 101;
            // 
            // lvSoLuong
            // 
            this.lvSoLuong.Text = "Số lượng";
            this.lvSoLuong.Width = 110;
            // 
            // lvThanhTien
            // 
            this.lvThanhTien.Text = "Thành tiền";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtTongTien);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Location = new System.Drawing.Point(13, 355);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(247, 42);
            this.panel3.TabIndex = 1;
            // 
            // txtTongTien
            // 
            this.txtTongTien.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTongTien.Enabled = false;
            this.txtTongTien.Location = new System.Drawing.Point(94, 5);
            this.txtTongTien.Name = "txtTongTien";
            this.txtTongTien.ReadOnly = true;
            this.txtTongTien.Size = new System.Drawing.Size(131, 30);
            this.txtTongTien.TabIndex = 0;
            this.txtTongTien.TabStop = false;
            this.txtTongTien.TextChanged += new System.EventHandler(this.txtTongTien_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tổng tiền";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tThanhToan);
            this.tabControl1.Controls.Add(this.tSanPham);
            this.tabControl1.Controls.Add(this.tKhachHang);
            this.tabControl1.Controls.Add(this.tQuanLy);
            this.tabControl1.Controls.Add(this.tTroGiup);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tabControl1.ImageList = this.imageList1;
            this.tabControl1.ItemSize = new System.Drawing.Size(155, 120);
            this.tabControl1.Location = new System.Drawing.Point(1, 1);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1040, 659);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 1;
            this.tabControl1.TabIndexChanged += new System.EventHandler(this.tabControl1_TabIndexChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 627);
            this.ControlBox = false;
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quản trị";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.tTroGiup.ResumeLayout(false);
            this.tTroGiup.PerformLayout();
            this.tQuanLy.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tKhachHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKH)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tSanPham.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSoluong)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSanPham)).EndInit();
            this.tThanhToan.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabPage tTroGiup;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnDangXuat;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabPage tQuanLy;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage tKhachHang;
        private System.Windows.Forms.DataGridView dgvKH;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnTimTenKH;
        private System.Windows.Forms.TextBox lTimTenKH;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lKhuVucKH;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lLTVKH;
        private System.Windows.Forms.Label lSDTKH;
        private System.Windows.Forms.Label lHoTenKH;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lalala;
        private System.Windows.Forms.TabPage tSanPham;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ComboBox cboDong;
        private System.Windows.Forms.ComboBox cboHang;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lDong;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lDonGia;
        private System.Windows.Forms.Label lHang;
        private System.Windows.Forms.Label lTenSanPham;
        private System.Windows.Forms.Label lMaSanPham;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvSanPham;
        private System.Windows.Forms.TabPage tThanhToan;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboPhanTram;
        private System.Windows.Forms.Label lThoiGian;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ListView lvHoaDon;
        private System.Windows.Forms.ColumnHeader lvMSP;
        private System.Windows.Forms.ColumnHeader lvTSP;
        private System.Windows.Forms.ColumnHeader lvHang;
        private System.Windows.Forms.ColumnHeader lvGia;
        private System.Windows.Forms.ColumnHeader lvSoLuong;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtTongTien;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnThanhToan;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.DataGridViewTextBoxColumn maSanPham;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenSanPham;
        private System.Windows.Forms.DataGridViewTextBoxColumn dongSanPham;
        private System.Windows.Forms.DataGridViewTextBoxColumn hangSanPham;
        private System.Windows.Forms.DataGridViewTextBoxColumn giaSanPham;
        private System.Windows.Forms.Button btnTimKiem;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenKhachHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn SDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn diaChiKhachHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn loaiThanhVien;
        private System.Windows.Forms.Button btnKhachHang;
        private System.Windows.Forms.Button btnSanPham;
        private SanPham sanPham1;
        private KhachHang khachHang2;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnThoatSP;
        private System.Windows.Forms.Button btnThoatKH;
        private System.Windows.Forms.Button btnThoatQL;
        private System.Windows.Forms.Button btnLamMoi;
        private System.Windows.Forms.ColumnHeader STT;
        private System.Windows.Forms.NumericUpDown numSoluong;
        private System.Windows.Forms.Label labSoLuong;
        private System.Windows.Forms.ColumnHeader lvThanhTien;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labHoTenKH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
    }
}
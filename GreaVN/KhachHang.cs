﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;
namespace GreaVN
{
    public partial class KhachHang : UserControl
    {
        BUS_KhachHang bus_khachhang = new BUS_KhachHang();


        private static KhachHang _instance;
        public static KhachHang Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new KhachHang();
                return _instance;
            }
        }
        public KhachHang()
        {
            InitializeComponent();
        }

        private void uc_Module1_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void KhachHang_Load(object sender, EventArgs e)
        {
            txtID.ReadOnly = true;
            dgvKHuserControl.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            //Hiện dữ liệu khách hàng
            dgvKHuserControl.DataSource = bus_khachhang.Get_KhachHang();
        }

        private void dgvKHuserControl_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                // txtSPma.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtID.Text = dgvKHuserControl.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtTen.Text = dgvKHuserControl.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtSDT.Text = dgvKHuserControl.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtDiaChi.Text = dgvKHuserControl.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtLoaiKhach.Text = dgvKHuserControl.Rows[e.RowIndex].Cells[4].Value.ToString();
            }
        }

        private void btnThemQL_Click(object sender, EventArgs e)
        {
            string tenKhachHang = txtTen.Text.Trim();
            string SDTKhachHang = txtSDT.Text.Trim();
            string diaChiKhachHang = txtDiaChi.Text.Trim();
            string loaiKhachHang = txtLoaiKhach.Text.Trim();
            int loai = Int32.Parse(loaiKhachHang);
            if(loai > 1 || loai < 0)
            {
                MessageBox.Show("Loại khách chỉ được 0 hoặc 1", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (tenKhachHang == "" || SDTKhachHang == "" || diaChiKhachHang == "" || loaiKhachHang == "")
            {

                MessageBox.Show("Thêm khách hàng không thành công", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvKHuserControl.DataSource = bus_khachhang.InsertKH(tenKhachHang, SDTKhachHang, diaChiKhachHang, loaiKhachHang);

                dgvKHuserControl.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                //Hiện thị dữ liệu sản phẩm
                dgvKHuserControl.DataSource = bus_khachhang.Get_KhachHang();

                MessageBox.Show("Thêm mới khách hàng thành công");
            }
        }

        private void btnCapNhatQL_Click(object sender, EventArgs e)
        {
            string maID = txtID.Text.Trim();
            string tenKhachHang = txtTen.Text.Trim();
            string SDTKhachHang = txtSDT.Text.Trim();
            string diaChiKhachHang = txtDiaChi.Text.Trim();
            string loaiKhachHang = txtLoaiKhach.Text.Trim();
            if (tenKhachHang == "" || SDTKhachHang == "" || diaChiKhachHang == "" || loaiKhachHang == "")
            {
                MessageBox.Show("Cập nhật không thành công", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvKHuserControl.DataSource = bus_khachhang.UpdateKH(maID,tenKhachHang, SDTKhachHang, diaChiKhachHang, loaiKhachHang);

                dgvKHuserControl.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                //Hiện thị dữ liệu sản phẩm
                dgvKHuserControl.DataSource = bus_khachhang.Get_KhachHang();

                MessageBox.Show("Cập nhật thành công");
            }
        }

        private void btnXoaQL_Click(object sender, EventArgs e)
        {
            string maID = txtID.Text.Trim();
            if (maID == "")
            {
                MessageBox.Show("Xóa Khách hàng không thành công", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvKHuserControl.DataSource = bus_khachhang.DeleteKH(maID);

                dgvKHuserControl.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                // Hiện thị dữ liệu sản phẩm
                dgvKHuserControl.DataSource = bus_khachhang.Get_KhachHang();
                txtTen.Text = "";
                txtDiaChi.Text = "";
                txtSDT.Text = "";
                txtLoaiKhach.Text = "";
                txtID.Text = "";
                MessageBox.Show("Xóa khách hàng thành công");
            }
        }

        private void txtLoaiKhach_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }
    }
}

﻿namespace GreaVN
{
    partial class KhachHang
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtLoaiKhach = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtSDT = new System.Windows.Forms.TextBox();
            this.txtTen = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.ID = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnXoaQL = new System.Windows.Forms.Button();
            this.btnCapNhatQL = new System.Windows.Forms.Button();
            this.btnThemQL = new System.Windows.Forms.Button();
            this.dgvKHuserControl = new System.Windows.Forms.DataGridView();
            this.ids = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenKhachHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SDT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.diaChiKhachHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loaiThanhVien = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvKHuserControl)).BeginInit();
            this.SuspendLayout();
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtLoaiKhach);
            this.panel9.Controls.Add(this.label1);
            this.panel9.Controls.Add(this.txtDiaChi);
            this.panel9.Controls.Add(this.txtSDT);
            this.panel9.Controls.Add(this.txtTen);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.txtID);
            this.panel9.Controls.Add(this.ID);
            this.panel9.Location = new System.Drawing.Point(3, 24);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(348, 261);
            this.panel9.TabIndex = 22;
            // 
            // txtLoaiKhach
            // 
            this.txtLoaiKhach.Location = new System.Drawing.Point(91, 199);
            this.txtLoaiKhach.Name = "txtLoaiKhach";
            this.txtLoaiKhach.Size = new System.Drawing.Size(230, 22);
            this.txtLoaiKhach.TabIndex = 30;
            this.txtLoaiKhach.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLoaiKhach_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 202);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 29;
            this.label1.Text = "Loại khách";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(91, 153);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(230, 22);
            this.txtDiaChi.TabIndex = 28;
            // 
            // txtSDT
            // 
            this.txtSDT.Location = new System.Drawing.Point(91, 109);
            this.txtSDT.Name = "txtSDT";
            this.txtSDT.Size = new System.Drawing.Size(230, 22);
            this.txtSDT.TabIndex = 27;
            // 
            // txtTen
            // 
            this.txtTen.Location = new System.Drawing.Point(91, 66);
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(230, 22);
            this.txtTen.TabIndex = 26;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 153);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 17);
            this.label17.TabIndex = 24;
            this.label17.Text = "Địa chỉ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 112);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 17);
            this.label16.TabIndex = 23;
            this.label16.Text = "SĐT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 69);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 17);
            this.label15.TabIndex = 22;
            this.label15.Text = "Tên ";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(91, 21);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(230, 22);
            this.txtID.TabIndex = 21;
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Location = new System.Drawing.Point(7, 21);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(21, 17);
            this.ID.TabIndex = 20;
            this.ID.Text = "ID";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnXoaQL);
            this.panel10.Controls.Add(this.btnCapNhatQL);
            this.panel10.Controls.Add(this.btnThemQL);
            this.panel10.Location = new System.Drawing.Point(18, 303);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(287, 38);
            this.panel10.TabIndex = 27;
            // 
            // btnXoaQL
            // 
            this.btnXoaQL.ForeColor = System.Drawing.Color.Red;
            this.btnXoaQL.Location = new System.Drawing.Point(209, 3);
            this.btnXoaQL.Name = "btnXoaQL";
            this.btnXoaQL.Size = new System.Drawing.Size(75, 30);
            this.btnXoaQL.TabIndex = 2;
            this.btnXoaQL.Text = "Xóa";
            this.btnXoaQL.UseVisualStyleBackColor = true;
            this.btnXoaQL.Click += new System.EventHandler(this.btnXoaQL_Click);
            // 
            // btnCapNhatQL
            // 
            this.btnCapNhatQL.ForeColor = System.Drawing.Color.Black;
            this.btnCapNhatQL.Location = new System.Drawing.Point(95, 3);
            this.btnCapNhatQL.Name = "btnCapNhatQL";
            this.btnCapNhatQL.Size = new System.Drawing.Size(97, 30);
            this.btnCapNhatQL.TabIndex = 1;
            this.btnCapNhatQL.Text = "Cập nhật";
            this.btnCapNhatQL.UseVisualStyleBackColor = true;
            this.btnCapNhatQL.Click += new System.EventHandler(this.btnCapNhatQL_Click);
            // 
            // btnThemQL
            // 
            this.btnThemQL.ForeColor = System.Drawing.Color.Blue;
            this.btnThemQL.Location = new System.Drawing.Point(3, 3);
            this.btnThemQL.Name = "btnThemQL";
            this.btnThemQL.Size = new System.Drawing.Size(75, 30);
            this.btnThemQL.TabIndex = 0;
            this.btnThemQL.Text = "Thêm";
            this.btnThemQL.UseVisualStyleBackColor = true;
            this.btnThemQL.Click += new System.EventHandler(this.btnThemQL_Click);
            // 
            // dgvKHuserControl
            // 
            this.dgvKHuserControl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvKHuserControl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ids,
            this.tenKhachHang,
            this.SDT,
            this.diaChiKhachHang,
            this.loaiThanhVien});
            this.dgvKHuserControl.Location = new System.Drawing.Point(337, 27);
            this.dgvKHuserControl.Name = "dgvKHuserControl";
            this.dgvKHuserControl.RowHeadersWidth = 51;
            this.dgvKHuserControl.RowTemplate.Height = 24;
            this.dgvKHuserControl.Size = new System.Drawing.Size(684, 296);
            this.dgvKHuserControl.TabIndex = 26;
            this.dgvKHuserControl.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvKHuserControl_CellClick);
            this.dgvKHuserControl.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // ids
            // 
            this.ids.DataPropertyName = "ID";
            this.ids.HeaderText = "ID";
            this.ids.MinimumWidth = 6;
            this.ids.Name = "ids";
            this.ids.Width = 125;
            // 
            // tenKhachHang
            // 
            this.tenKhachHang.DataPropertyName = "tenKhachHang";
            this.tenKhachHang.HeaderText = "Tên khách hàng";
            this.tenKhachHang.MinimumWidth = 6;
            this.tenKhachHang.Name = "tenKhachHang";
            this.tenKhachHang.Width = 125;
            // 
            // SDT
            // 
            this.SDT.DataPropertyName = "SDT";
            this.SDT.HeaderText = "Số điện thoại";
            this.SDT.MinimumWidth = 6;
            this.SDT.Name = "SDT";
            this.SDT.Width = 125;
            // 
            // diaChiKhachHang
            // 
            this.diaChiKhachHang.DataPropertyName = "diaChiKhachHang";
            this.diaChiKhachHang.HeaderText = "Địa chỉ";
            this.diaChiKhachHang.MinimumWidth = 6;
            this.diaChiKhachHang.Name = "diaChiKhachHang";
            this.diaChiKhachHang.Width = 125;
            // 
            // loaiThanhVien
            // 
            this.loaiThanhVien.DataPropertyName = "loaiThanhVien";
            this.loaiThanhVien.HeaderText = "Loại Khách";
            this.loaiThanhVien.MinimumWidth = 6;
            this.loaiThanhVien.Name = "loaiThanhVien";
            this.loaiThanhVien.Width = 125;
            // 
            // KhachHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.dgvKHuserControl);
            this.Controls.Add(this.panel9);
            this.Name = "KhachHang";
            this.Size = new System.Drawing.Size(1032, 381);
            this.Load += new System.EventHandler(this.KhachHang_Load);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvKHuserControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.TextBox txtSDT;
        private System.Windows.Forms.TextBox txtTen;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnXoaQL;
        private System.Windows.Forms.Button btnCapNhatQL;
        private System.Windows.Forms.Button btnThemQL;
        private System.Windows.Forms.DataGridView dgvKHuserControl;
        private System.Windows.Forms.DataGridViewTextBoxColumn ids;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenKhachHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn SDT;
        private System.Windows.Forms.DataGridViewTextBoxColumn diaChiKhachHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn loaiThanhVien;
        private System.Windows.Forms.TextBox txtLoaiKhach;
        private System.Windows.Forms.Label label1;
    }
}

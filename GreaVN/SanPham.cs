﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using DTO;
using System.Data.SqlClient;

namespace GreaVN
{
    public partial class SanPham : UserControl
    {
        BUS_SanPham bus_sanpham = new BUS_SanPham();
        
        //SanPham sp = new SanPham();
        public SanPham()
        {
            InitializeComponent();
        }

        private void panel9_Paint(object sender, PaintEventArgs e)
        {
        }

        private void SanPham_Load(object sender, EventArgs e)
        {
            txtSPma.ReadOnly = true;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            //Hiện thị dữ liệu sản phẩm
            dataGridView1.DataSource = bus_sanpham.Get_SanPham();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
               // txtSPma.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtSpTen.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtSpDong.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtSpHang.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtSpGia.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();   
                txtSPma.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            }
        }

        private void btnCapNhatQL_Click(object sender, EventArgs e)
        {
            
            string maSanPham = txtSPma.Text.Trim();
            string tenSanPham = txtSpTen.Text.Trim();
            string hangSanPham = txtSpHang.Text.Trim();
            string dongSanPham = txtSpDong.Text.Trim();
            string giaSanPham = txtSpGia.Text.Trim();
            if (tenSanPham == "" || hangSanPham == "" || dongSanPham == "" || giaSanPham == "")
            {
                MessageBox.Show("Sửa sản phẩm không thành công", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dataGridView1.DataSource = bus_sanpham.UpdateSP(maSanPham,tenSanPham, hangSanPham, dongSanPham, giaSanPham);

                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
               // Hiện thị dữ liệu sản phẩm
                dataGridView1.DataSource = bus_sanpham.Get_SanPham();

                MessageBox.Show("Sửa sản phẩm thành công");
            }
        }

        private void btnThemQL_Click(object sender, EventArgs e)
        {
            string tenSanPham = txtSpTen.Text.Trim();
            string hangSanPham = txtSpHang.Text.Trim();
            string dongSanPham = txtSpDong.Text.Trim();
            string giaSanPham = txtSpGia.Text.Trim();
            if(tenSanPham == "" || hangSanPham == "" || dongSanPham == "" || giaSanPham=="")
            {
                MessageBox.Show("Thêm sản phẩm không thành công", "Thông báo!", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                dataGridView1.DataSource = bus_sanpham.InsertSP(tenSanPham, hangSanPham, dongSanPham, giaSanPham);

                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                //Hiện thị dữ liệu sản phẩm
                dataGridView1.DataSource = bus_sanpham.Get_SanPham();

                MessageBox.Show("Thêm mới sản phẩm thành công");
            }
            
        }

        private void txtSpGia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void btnXoaQL_Click(object sender, EventArgs e)
        {
            string maSanPham = txtSPma.Text.Trim();
            if (maSanPham == "")
            {
                MessageBox.Show("Xóa không thành công", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dataGridView1.DataSource = bus_sanpham.DeleteSP(maSanPham);

                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                // Hiện thị dữ liệu sản phẩm
                dataGridView1.DataSource = bus_sanpham.Get_SanPham();
                txtSpTen.Text = "";
                txtSpDong.Text = "";
                txtSpHang.Text = "";
                txtSpGia.Text = "";
                txtSPma.Text = "";
                MessageBox.Show("Xóa sản phẩm thành công");
            }
           
        }
    }
}

﻿namespace GreaVN
{
    partial class SanPham
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtSpGia = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSpHang = new System.Windows.Forms.TextBox();
            this.txtSpDong = new System.Windows.Forms.TextBox();
            this.txtSpTen = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSPma = new System.Windows.Forms.TextBox();
            this.ID = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnXoaQL = new System.Windows.Forms.Button();
            this.btnCapNhatQL = new System.Windows.Forms.Button();
            this.btnThemQL = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.maSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tenSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hangSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dongSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.giaSanPham = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtSpGia);
            this.panel9.Controls.Add(this.label1);
            this.panel9.Controls.Add(this.txtSpHang);
            this.panel9.Controls.Add(this.txtSpDong);
            this.panel9.Controls.Add(this.txtSpTen);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.label15);
            this.panel9.Controls.Add(this.txtSPma);
            this.panel9.Controls.Add(this.ID);
            this.panel9.Location = new System.Drawing.Point(3, 24);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(348, 261);
            this.panel9.TabIndex = 23;
            // 
            // txtSpGia
            // 
            this.txtSpGia.Location = new System.Drawing.Point(91, 199);
            this.txtSpGia.Name = "txtSpGia";
            this.txtSpGia.Size = new System.Drawing.Size(230, 22);
            this.txtSpGia.TabIndex = 30;
            this.txtSpGia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSpGia_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 29;
            this.label1.Text = "Giá";
            // 
            // txtSpHang
            // 
            this.txtSpHang.Location = new System.Drawing.Point(91, 153);
            this.txtSpHang.Name = "txtSpHang";
            this.txtSpHang.Size = new System.Drawing.Size(230, 22);
            this.txtSpHang.TabIndex = 28;
            // 
            // txtSpDong
            // 
            this.txtSpDong.Location = new System.Drawing.Point(91, 109);
            this.txtSpDong.Name = "txtSpDong";
            this.txtSpDong.Size = new System.Drawing.Size(230, 22);
            this.txtSpDong.TabIndex = 27;
            // 
            // txtSpTen
            // 
            this.txtSpTen.Location = new System.Drawing.Point(91, 66);
            this.txtSpTen.Name = "txtSpTen";
            this.txtSpTen.Size = new System.Drawing.Size(230, 22);
            this.txtSpTen.TabIndex = 26;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 153);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 17);
            this.label17.TabIndex = 24;
            this.label17.Text = "Hãng SX";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 109);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 17);
            this.label16.TabIndex = 23;
            this.label16.Text = "Dòng SP";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 66);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 17);
            this.label15.TabIndex = 22;
            this.label15.Text = "Tên ";
            // 
            // txtSPma
            // 
            this.txtSPma.Location = new System.Drawing.Point(91, 21);
            this.txtSPma.Name = "txtSPma";
            this.txtSPma.Size = new System.Drawing.Size(230, 22);
            this.txtSPma.TabIndex = 21;
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Location = new System.Drawing.Point(7, 21);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(49, 17);
            this.ID.TabIndex = 20;
            this.ID.Text = "Mã SP";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnXoaQL);
            this.panel10.Controls.Add(this.btnCapNhatQL);
            this.panel10.Controls.Add(this.btnThemQL);
            this.panel10.Location = new System.Drawing.Point(18, 303);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(287, 38);
            this.panel10.TabIndex = 25;
            // 
            // btnXoaQL
            // 
            this.btnXoaQL.ForeColor = System.Drawing.Color.Red;
            this.btnXoaQL.Location = new System.Drawing.Point(209, 3);
            this.btnXoaQL.Name = "btnXoaQL";
            this.btnXoaQL.Size = new System.Drawing.Size(75, 30);
            this.btnXoaQL.TabIndex = 2;
            this.btnXoaQL.Text = "Xóa";
            this.btnXoaQL.UseVisualStyleBackColor = true;
            this.btnXoaQL.Click += new System.EventHandler(this.btnXoaQL_Click);
            // 
            // btnCapNhatQL
            // 
            this.btnCapNhatQL.ForeColor = System.Drawing.Color.Black;
            this.btnCapNhatQL.Location = new System.Drawing.Point(95, 3);
            this.btnCapNhatQL.Name = "btnCapNhatQL";
            this.btnCapNhatQL.Size = new System.Drawing.Size(97, 30);
            this.btnCapNhatQL.TabIndex = 1;
            this.btnCapNhatQL.Text = "Cập nhật";
            this.btnCapNhatQL.UseVisualStyleBackColor = true;
            this.btnCapNhatQL.Click += new System.EventHandler(this.btnCapNhatQL_Click);
            // 
            // btnThemQL
            // 
            this.btnThemQL.ForeColor = System.Drawing.Color.Blue;
            this.btnThemQL.Location = new System.Drawing.Point(3, 3);
            this.btnThemQL.Name = "btnThemQL";
            this.btnThemQL.Size = new System.Drawing.Size(75, 30);
            this.btnThemQL.TabIndex = 0;
            this.btnThemQL.Text = "Thêm";
            this.btnThemQL.UseVisualStyleBackColor = true;
            this.btnThemQL.Click += new System.EventHandler(this.btnThemQL_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.maSanPham,
            this.tenSanPham,
            this.hangSanPham,
            this.dongSanPham,
            this.giaSanPham});
            this.dataGridView1.Location = new System.Drawing.Point(337, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(684, 296);
            this.dataGridView1.TabIndex = 24;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // maSanPham
            // 
            this.maSanPham.DataPropertyName = "maSanPham";
            this.maSanPham.HeaderText = "Mã sản phẩm";
            this.maSanPham.MinimumWidth = 6;
            this.maSanPham.Name = "maSanPham";
            this.maSanPham.Width = 125;
            // 
            // tenSanPham
            // 
            this.tenSanPham.DataPropertyName = "tenSanPham";
            this.tenSanPham.HeaderText = "Tên sản phẩm";
            this.tenSanPham.MinimumWidth = 6;
            this.tenSanPham.Name = "tenSanPham";
            this.tenSanPham.Width = 125;
            // 
            // hangSanPham
            // 
            this.hangSanPham.DataPropertyName = "hangSanPham";
            this.hangSanPham.HeaderText = "Hãng sản phẩm";
            this.hangSanPham.MinimumWidth = 6;
            this.hangSanPham.Name = "hangSanPham";
            this.hangSanPham.Width = 125;
            // 
            // dongSanPham
            // 
            this.dongSanPham.DataPropertyName = "dongSanPham";
            this.dongSanPham.HeaderText = "Dòng sản phẩm";
            this.dongSanPham.MinimumWidth = 6;
            this.dongSanPham.Name = "dongSanPham";
            this.dongSanPham.Width = 125;
            // 
            // giaSanPham
            // 
            this.giaSanPham.DataPropertyName = "giaSanPham";
            this.giaSanPham.HeaderText = "Giá";
            this.giaSanPham.MinimumWidth = 6;
            this.giaSanPham.Name = "giaSanPham";
            this.giaSanPham.Width = 125;
            // 
            // SanPham
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.panel9);
            this.Name = "SanPham";
            this.Size = new System.Drawing.Size(1032, 381);
            this.Load += new System.EventHandler(this.SanPham_Load);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox txtSpHang;
        private System.Windows.Forms.TextBox txtSpDong;
        private System.Windows.Forms.TextBox txtSpTen;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSPma;
        private System.Windows.Forms.Label ID;
        private System.Windows.Forms.TextBox txtSpGia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button btnXoaQL;
        private System.Windows.Forms.Button btnCapNhatQL;
        private System.Windows.Forms.Button btnThemQL;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn maSanPham;
        private System.Windows.Forms.DataGridViewTextBoxColumn tenSanPham;
        private System.Windows.Forms.DataGridViewTextBoxColumn hangSanPham;
        private System.Windows.Forms.DataGridViewTextBoxColumn dongSanPham;
        private System.Windows.Forms.DataGridViewTextBoxColumn giaSanPham;
    }
}

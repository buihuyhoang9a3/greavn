﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using BUS;
using DTO;


namespace GreaVN
{
    public partial class Form2 : Form
    {
        BUS_SanPham bus_sanpham = new BUS_SanPham();
        BUS_KhachHang bus_khachhang = new BUS_KhachHang();

        public DTO.KhachHang dtoKhachHang = new DTO.KhachHang();



        SanPham sp = new SanPham();
        private object panel;
        private double tongtien;
        public Form2()  
        {
            InitializeComponent();
        }
        private void timer1_Tick_1(object sender, EventArgs e)
        {
            int gio = DateTime.Now.Hour;
            int phut = DateTime.Now.Minute;
            int giay = DateTime.Now.Second;

            lThoiGian.Text = gio + ":" + phut + ":" + giay;
            timer1.Start();
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            
        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {

            DialogResult h = MessageBox.Show
                ("Bạn có chắc muốn thoát không?", "Error", MessageBoxButtons.OKCancel);
            if (h == DialogResult.OK)
                Application.Exit();
        }

        private void btnThanhToan_Click_1(object sender, EventArgs e)
        {
            if(this._dsSanPham.Count>0)
            {
                DTO.HoaDon hds = new DTO.HoaDon();

                hds.MaKhachHang = this.dtoKhachHang.Makhachhang;
                hds.NgayLapDon = DateTime.Today.ToShortDateString();
                hds.ThoiGian = DateTime.Now.ToShortTimeString();
                hds.TongTien = this.tongtien.ToString();
                BUS_HoaDon bhd = new BUS_HoaDon();
                if(labHoTenKH.Text.Length>0)
                {
                   if (bhd.InsertHoaDon(hds))
                    {
                        string mahd = bhd.LayLastID();
                        foreach(DTO.SanPham i in this._dsSanPham)
                        {
                            bhd.InsertChiTietHoaDon(i, mahd);
                        }
                        MessageBox.Show("Thanh toán thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                        Form3 f3 = new Form3();
                        f3._mahd = mahd;
                        f3.ShowDialog();
                        this._dsSanPham.Clear();
                        CapNhatHoaDon();
                    }   
                   else
                    {
                        MessageBox.Show("Thêm thất bại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }    
                    
                }  
                else
                {
                    MessageBox.Show("Chưa nhập số điện thoại khách hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }    
            }   
            else
            {
                MessageBox.Show("Bạn chưa đặt hàng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }    
        }

        private void btnDangXuat_Click_1(object sender, EventArgs e)
        {
            
            DialogResult h = MessageBox.Show
                ("Bạn có chắc muốn thoát không?", "Error", MessageBoxButtons.OKCancel);
            if (h == DialogResult.OK)
                Application.Exit();
            //Form1 f = new Form1();
            //this.Hide();
            //f.ShowDialog();
            //this.Show();
        }

        private void lThoiGian_Click(object sender, EventArgs e)
        {
            int gio = DateTime.Now.Hour;
            int phut = DateTime.Now.Minute;
            int giay = DateTime.Now.Second;

            lThoiGian.Text = gio + ":" + phut + ":" + giay;
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {
           // txtSDT.Text = "0911784363";
            timer1.Start();

            //Hiện thị lên combobox Mã Sản phẩm
            cboHang.DataSource = bus_sanpham.Get_maSanPham();
           
            cboHang.DisplayMember = "Hang";
            cboHang.ValueMember = "ID";


            //Hiện thị lên combobox Dòng Sản phẩm
            cboDong.DataSource = bus_sanpham.Get_dongSanPham();
            cboDong.DisplayMember = "Dong";
            cboDong.ValueMember = "ID";

            //Hiện thị dữ liệu sản phẩm
            dgvSanPham.DataSource = bus_sanpham.Get_SanPham();


            //Hiện dữ liệu khách hàng
            dgvKH.DataSource = bus_khachhang.Get_KhachHang();



        }

        private void dgvSanPham_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex >=0)
            {
                lTenSanPham.Text = dgvSanPham.Rows[e.RowIndex].Cells[0].Value.ToString();
                lHang.Text = dgvSanPham.Rows[e.RowIndex].Cells[1].Value.ToString();
                lDong.Text = dgvSanPham.Rows[e.RowIndex].Cells[2].Value.ToString();
                lDonGia.Text = dgvSanPham.Rows[e.RowIndex].Cells[3].Value.ToString();
                lMaSanPham.Text = dgvSanPham.Rows[e.RowIndex].Cells[4].Value.ToString();
                numSoluong.Value = 1;
            }    
                

        }

        private void dgvSanPham_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            string dong = cboDong.Text.Trim();
            string hang = cboHang.Text.Trim();
            Console.WriteLine(dong + "0");
            Console.WriteLine(hang + "0");
            dgvSanPham.DataSource = bus_sanpham.Search(dong,hang);
        }

        private void txtTongTien_TextChanged(object sender, EventArgs e)
        {

        }

        private void cboLoaiKH_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void cboHang_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void dgvKH_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                lHoTenKH.Text = dgvKH.Rows[e.RowIndex].Cells[1].Value.ToString();
                lSDTKH.Text = dgvKH.Rows[e.RowIndex].Cells[2].Value.ToString();
                lLTVKH.Text = dgvKH.Rows[e.RowIndex].Cells[4].Value.ToString();
                lKhuVucKH.Text = dgvKH.Rows[e.RowIndex].Cells[3].Value.ToString();
            }
        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void btnTimTenKH_Click(object sender, EventArgs e)
        {
            string ten = lTimTenKH.Text.Trim();
            dgvKH.DataSource = bus_sanpham.SearchKH(ten);
        }

        private void lTimTenKH_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnKhachHang_Click(object sender, EventArgs e)
        {
            khachHang2.BringToFront();

            //Hiện dữ liệu khách hàng


            //dataGridView1.Columns[0].HeaderText = "ID";
            //dataGridView1.Columns[1].HeaderText = "Họ và tên";
            //dataGridView1.Columns[2].HeaderText = "SĐT";
            //dataGridView1.Columns[3].HeaderText = "Địa chỉ";

            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
           // dataGridView1.DataSource = bus_khachhang.Get_KhachHang();

           
            //dataGridView1.Columns.Add(string.Format("col{0}", 0), "ID");
            //dataGridView1.Columns.Add(string.Format("col{0}", 0), "Họ và tên");
            //dataGridView1.Columns.Add(string.Format("col{0}", 0), "SĐT");
            //dataGridView1.Columns.Add(string.Format("col{0}", 0), "Địa chỉ");
            //dataGridView1.Columns.Add(string.Format("col{0}", 0), "Loại thành viên");




        }

        private void btnSanPham_Click(object sender, EventArgs e)
        {
            sanPham1.BringToFront();

            //Hiện thị dữ liệu sản phẩm
           // dataGridView1.DataSource = bus_sanpham.Get_SanPham();
        }

        private void khachHang2_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //khachHang2.BringToFront();
           



        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult h = MessageBox.Show
                ("Bạn có chắc muốn thoát không?", "Error", MessageBoxButtons.OKCancel);
            if (h == DialogResult.OK)
                Application.Exit();
        }

        private void btnThoatSP_Click(object sender, EventArgs e)
        {
            DialogResult h = MessageBox.Show
               ("Bạn có chắc muốn thoát không?", "Error", MessageBoxButtons.OKCancel);
            if (h == DialogResult.OK)
                Application.Exit();
        }

        private void btnThoatKH_Click(object sender, EventArgs e)
        {
            DialogResult h = MessageBox.Show
               ("Bạn có chắc muốn thoát không?", "Error", MessageBoxButtons.OKCancel);
            if (h == DialogResult.OK)
                Application.Exit();
        }

        private void btnThoatQL_Click(object sender, EventArgs e)
        {
            DialogResult h = MessageBox.Show
               ("Bạn có chắc muốn thoát không?", "Error", MessageBoxButtons.OKCancel);
            if (h == DialogResult.OK)
                Application.Exit();
        }

        private void tKhachHang_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cboDong_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }
        private List<DTO.SanPham> _dsSanPham = new List<DTO.SanPham>();
        private void btnThem_Click(object sender, EventArgs e)
        {
            if(lMaSanPham.Text.Length>0)
            {
                if (dgvSanPham.SelectedRows.Count > 0)
                {
                    if(int.Parse(numSoluong.Value.ToString())>0)
                    {
                        if (KiemTraTrungMaTrongDanhSachSanPham(this._dsSanPham, lMaSanPham.Text, int.Parse(numSoluong.Value.ToString())))
                        {
                            MessageBox.Show("Thêm sản phẩm " + lMaSanPham.Text + " thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            string[] temp = lDonGia.Text.Split('.');
                            lDonGia.Text = temp[0];
                            DTO.SanPham sanphamOne = new DTO.SanPham(lMaSanPham.Text, lTenSanPham.Text, lHang.Text, lDong.Text, lDonGia.Text, int.Parse(numSoluong.Value.ToString()));
                            this._dsSanPham.Add(sanphamOne);
                            MessageBox.Show("Thêm sản phẩm " + lMaSanPham.Text + " thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }    
                    else
                    {
                        MessageBox.Show("Số lượng phải lớn hơn 0", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }    
                    
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn sản phẩm!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }    
        }
        private bool KiemTraTrungMaTrongDanhSachSanPham(List<DTO.SanPham> sp , string masp, int soluong)
        {
            int count = 0;
            foreach(DTO.SanPham i in sp)
            {
                if(i.Masanspham.Equals(masp))
                {
                    i.Soluong += soluong;
                    return true;
                }    
            }
            return false;
        }
        private void tabControl1_TabIndexChanged(object sender, EventArgs e)
        {
            MessageBox.Show("chuyển tab");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this._dsSanPham[0].Masanspham);
        }
        private void CapNhatHoaDon()
        {
            lvHoaDon.Items.Clear();
            int i = 1;
            double sum = 0;
            foreach(DTO.SanPham sp in this._dsSanPham)
            {
                ListViewItem item = new ListViewItem();
                item.SubItems[0].Text = i.ToString();
                item.SubItems.Add(sp.Masanspham);
                item.SubItems.Add(sp.Tensanpham);
                item.SubItems.Add(sp.Hangsanpham);
                item.SubItems.Add(sp.Giasanpham);
                item.SubItems.Add(sp.Soluong.ToString());
                int temp = sp.Soluong * int.Parse(sp.Giasanpham.ToString());
                sum += temp;
                item.SubItems.Add(temp.ToString());
                lvHoaDon.Items.Add(item);
                i++;
            }
            int donVi = 0;
            if(int.TryParse(cboPhanTram.Text, out donVi))
            {
                float phanTram = (float)donVi/ 100;
                sum -= (sum * phanTram);
            }    
            else
            {
                cboPhanTram.Text = "0";
            }
            this.tongtien = sum;
            txtTongTien.Text = sum.ToString("#,##0" + " VNĐ");
            
        }
        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            CapNhatHoaDon();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if(lvHoaDon.SelectedItems.Count>0)
            {
                foreach(ListViewItem item in lvHoaDon.SelectedItems)
                {
                    Xoa1SanPham(this._dsSanPham, item.SubItems[1].Text);
                }
                CapNhatHoaDon();
            }   
            else
            {
                MessageBox.Show("Vui lòng chọn sản phẩm", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }
        private void Xoa1SanPham(List<DTO.SanPham> sp, string masp)
        {
            foreach(DTO.SanPham i in sp)
            {
                if(i.Masanspham.Equals(masp))
                {
                    sp.Remove(i);
                    break;
                }    
            }    
        }
        private void cboLoaiKH_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void labHTAdmin_Click(object sender, EventArgs e)
        {

        }

        private void txtSDT_TextChanged(object sender, EventArgs e)
        {
            if(txtSDT.Text.Length >= 10)
            {
                DataTable dt = new DataTable();
                dt = bus_khachhang.Get_KhachHang_SDT(txtSDT.Text);
                if(dt.Rows.Count>0)
                {
                    labHoTenKH.Text = dt.Rows[0]["tenKhachHang"].ToString();
                    this.dtoKhachHang.Tenkhachhang = labHoTenKH.Text;
                    this.dtoKhachHang.Makhachhang = dt.Rows[0]["ID"].ToString();
                }    
            } 
            
        }

        private void txtSDT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
                e.Handled = true;
        }
    }

}

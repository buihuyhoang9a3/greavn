﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using System.Text;
using DTO;

namespace BUS
{
    public class BUS_Admin
    {
        DAL_Admin admin = new DAL_Admin();
        
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        
        public DTO.Admin Verify(String userName, String password)
        {
            return admin.Verify(userName, CreateMD5(password));
        }

        public Exception Create(string userName, string password, string level)
        {
            return admin.Create(userName, CreateMD5(password), level);
        }
    }
}

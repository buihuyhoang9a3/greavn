﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using System.Data;
namespace BUS
{
    public class BUS_HoaDon
    {
        private DAL_HoaDoncs hds = new DAL_HoaDoncs();
        public string LayLastID()
        {
            return this.hds.LayLastID();
        }
        public bool InsertHoaDon(DTO.HoaDon hd)
        {
            return this.hds.InsertHoaDon(hd);
        }
        public bool InsertChiTietHoaDon(DTO.SanPham sp, string maHD)
        {
            return this.hds.InsertChiTietHoaDon(sp, maHD);
        }
        public DataSet LayThongTinHoaDon(string mahd)
        {
            return this.hds.LayThongTinHoaDon(mahd);
        }
    }
}

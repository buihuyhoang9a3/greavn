﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using System.Data;

namespace BUS
{
    public  class BUS_SanPham
    {
        DAL_SanPham dal_sanpham = new DAL_SanPham();
        public DataTable Get_maSanPham()
        {
            return dal_sanpham.Get_maSanPham(); 
        }
        public DataTable Get_dongSanPham()
        {
            return dal_sanpham.Get_dongSanPham();
        }
        public DataTable Get_SanPham()
        {
            return dal_sanpham.Get_SanPham();
        }
        public DataTable Search(string hang, string dong)
        {
            return dal_sanpham.Search(hang,dong);
        }
        public DataTable SearchKH(string ten)
        {
            return dal_sanpham.SearchKH(ten);
        }
        public DataTable InsertSP(string tenSP, string hangSP, string dongSP, string giaSP)
        {
            return dal_sanpham.InsertSP(tenSP, hangSP, dongSP, giaSP);
        }
        public DataTable UpdateSP(string maSP, string tenSP, string hangSP, string dongSP, string giaSP)
        {
            return dal_sanpham.UpdateSP(maSP, tenSP, hangSP, dongSP, giaSP);
        }
        public DataTable DeleteSP(string maSP)
        {
            return dal_sanpham.DeleteSP(maSP);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using System.Data;

namespace BUS
{
   public class BUS_KhachHang
    {
        DAL_KhachHang dal_khachhang = new DAL_KhachHang();
        public DataTable Get_KhachHang()
        {
            return dal_khachhang.Get_KhachHang();
        }
        public DataTable Get_KhachHang_SDT(string sdt)
        {
            return dal_khachhang.Get_KhachHang(sdt);
        }
        public DataTable InsertKH(string tenKH, string SDTKH, string diaChiKH, string loaiKH)
        {
            return dal_khachhang.InsertKH(tenKH,SDTKH,diaChiKH,loaiKH);
        }
        public DataTable UpdateKH(string maID, string tenKH, string SDTKH, string diaChiKH, string loaiKH)
        {
            return dal_khachhang.UpdateKH(maID, tenKH, SDTKH, diaChiKH, loaiKH);
        }
        public DataTable DeleteKH(string maID)
        {
            return dal_khachhang.DeleteKH(maID);
        }

    }
}

USE [GrearVN]
GO
/****** Object:  Table [dbo].[AdminGear]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminGear](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AdminName] [varchar](100) NOT NULL,
	[AdminPassword] [varchar](255) NOT NULL,
	[AdminLevel] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ChiTietHoaDon]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietHoaDon](
	[maHD] [int] NOT NULL,
	[soLuong] [int] NOT NULL,
	[thanhTien] [int] NOT NULL,
	[trangThai] [int] NOT NULL,
	[maSP] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DongSanPham]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DongSanPham](
	[Dong] [nchar](10) NOT NULL,
	[ID] [nchar](10) NOT NULL,
 CONSTRAINT [PK_DongSanPham] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HangSanPham]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HangSanPham](
	[Hang] [nchar](10) NULL,
	[ID] [int] NOT NULL,
 CONSTRAINT [PK_HangSanPham] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HoaDon](
	[maHD] [int] IDENTITY(1,1) NOT NULL,
	[maKhachHang] [int] NOT NULL,
	[ngayLapDon] [varchar](50) NULL,
	[thoiGian] [varchar](50) NULL,
	[tongTien] [int] NOT NULL,
	[trangThai] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[tenKhachHang] [nchar](50) NOT NULL,
	[SDT] [nchar](50) NOT NULL,
	[diaChiKhachHang] [nchar](50) NOT NULL,
	[loaiThanhVien] [int] NOT NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SanPham](
	[tenSanPham] [text] NOT NULL,
	[hangSanPham] [text] NOT NULL,
	[dongSanPham] [text] NOT NULL,
	[giaSanPham] [int] NULL,
	[maSanPham] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[maSanPham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[temp]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[temp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nchar](10) NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[AdminGear] ON 

INSERT [dbo].[AdminGear] ([ID], [AdminName], [AdminPassword], [AdminLevel]) VALUES (1, N'admin', N'827CCB0EEA8A706C4C34A16891F84E7B', 1)
SET IDENTITY_INSERT [dbo].[AdminGear] OFF
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (9, 5, 100000, 1, 1)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (18, 1, 652, 1, 5)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (19, 1, 652, 1, 5)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (19, 1, 16513510, 1, 13)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (35, 1, 2000000, 1, 3)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (35, 1, 100000, 1, 1)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (35, 1, 151020, 1, 7)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (36, 1, 1200000, 1, 2)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (19, 1, 16511110, 1, 16)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (19, 1, 165130, 1, 22)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (20, 1, 652, 1, 5)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (20, 1, 16513510, 1, 13)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (20, 1, 16511110, 1, 16)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (20, 7, 700000, 1, 28)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (21, 1, 120000, 1, 2)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (21, 1, 30000, 1, 4)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (22, 1, 653132, 1, 8)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (23, 1, 120000, 1, 2)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (24, 1, 30000, 1, 4)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (25, 1, 2000000, 1, 3)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (25, 1, 652, 1, 5)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (25, 1, 16513210, 1, 9)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (26, 1, 120000, 1, 2)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (26, 1, 652, 1, 5)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (26, 1, 100000, 1, 1)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (27, 1, 100000, 1, 1)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (27, 1, 120000, 1, 2)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (27, 1, 1651310, 1, 10)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (27, 6, 99066660, 1, 16)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (28, 3, 49540530, 1, 13)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (28, 1, 16511110, 1, 16)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (28, 3, 4954953, 1, 25)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (29, 1, 100000, 1, 1)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (30, 1, 120000, 1, 32)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (31, 1, 30000, 1, 4)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (32, 1, 652, 1, 5)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (33, 1, 120000, 1, 2)
INSERT [dbo].[ChiTietHoaDon] ([maHD], [soLuong], [thanhTien], [trangThai], [maSP]) VALUES (34, 1, 151020, 1, 7)
INSERT [dbo].[DongSanPham] ([Dong], [ID]) VALUES (N'Game      ', N'1         ')
INSERT [dbo].[DongSanPham] ([Dong], [ID]) VALUES (N'Graphics  ', N'2         ')
INSERT [dbo].[DongSanPham] ([Dong], [ID]) VALUES (N'Office    ', N'3         ')
INSERT [dbo].[HangSanPham] ([Hang], [ID]) VALUES (N'Asus      ', 1)
INSERT [dbo].[HangSanPham] ([Hang], [ID]) VALUES (N'Lenovo    ', 2)
INSERT [dbo].[HangSanPham] ([Hang], [ID]) VALUES (N'Dell      ', 3)
INSERT [dbo].[HangSanPham] ([Hang], [ID]) VALUES (N'HP        ', 4)
SET IDENTITY_INSERT [dbo].[HoaDon] ON 

INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (4, 0, N'18/07/2020', N'16:05', 1234, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (5, 0, N'18/07/2020', N'16:05', 1234, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (6, 1, N'18/07/2020', N'1:13 PM', 120000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (7, 1, N'18/07/2020', N'1:16 PM', 100000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (8, 1, N'18/07/2020', N'1:22 PM', 100000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (9, 1, N'18/07/2020', N'1:23 PM', 171651, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (35, 1, N'18/07/2020', N'4:09 PM', 2251020, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (36, 1, N'18/07/2020', N'4:11 PM', 0, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (10, 1, N'18/07/2020', N'1:35 PM', 16516350, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (11, 1, N'18/07/2020', N'1:37 PM', 62, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (12, 1, N'18/07/2020', N'1:38 PM', 120000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (13, 1, N'18/07/2020', N'1:39 PM', 652, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (14, 1, N'18/07/2020', N'1:41 PM', 2000000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (15, 1, N'18/07/2020', N'1:42 PM', 120000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (16, 1, N'18/07/2020', N'1:43 PM', 30000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (17, 1, N'18/07/2020', N'1:49 PM', 2000000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (18, 1, N'18/07/2020', N'1:52 PM', 652, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (21, 1, N'18/07/2020', N'1:56 PM', 150000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (19, 1, N'18/07/2020', N'1:53 PM', 33190402, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (20, 1, N'18/07/2020', N'1:54 PM', 33725272, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (22, 1, N'18/07/2020', N'3:37 PM', 653132, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (23, 1, N'18/07/2020', N'3:43 PM', 120000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (24, 1, N'18/07/2020', N'3:43 PM', 30000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (25, 1, N'18/07/2020', N'3:50 PM', 18513862, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (26, 1, N'18/07/2020', N'3:52 PM', 220652, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (27, 1, N'18/07/2020', N'3:54 PM', 100937970, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (28, 1, N'18/07/2020', N'3:57 PM', 71006593, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (29, 1, N'18/07/2020', N'3:58 PM', 100000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (30, 1, N'18/07/2020', N'3:59 PM', 120000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (31, 1, N'18/07/2020', N'4:00 PM', 30000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (32, 1, N'18/07/2020', N'4:01 PM', 652, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (33, 1, N'18/07/2020', N'4:03 PM', 120000, 1)
INSERT [dbo].[HoaDon] ([maHD], [maKhachHang], [ngayLapDon], [thoiGian], [tongTien], [trangThai]) VALUES (34, 1, N'18/07/2020', N'4:09 PM', 151020, 1)
SET IDENTITY_INSERT [dbo].[HoaDon] OFF
SET IDENTITY_INSERT [dbo].[KhachHang] ON 

INSERT [dbo].[KhachHang] ([ID], [tenKhachHang], [SDT], [diaChiKhachHang], [loaiThanhVien]) VALUES (1, N'Bùi Huy Hoàng                                     ', N'0911784363                                        ', N'91/8 Nguyễn Khoái Q4,TPHCM                        ', 1)
INSERT [dbo].[KhachHang] ([ID], [tenKhachHang], [SDT], [diaChiKhachHang], [loaiThanhVien]) VALUES (2, N'Từ Vĩ Khang                                       ', N'0911561516                                        ', N'91/8 Nguyễn Khoái Q4,TPHCM                        ', 1)
INSERT [dbo].[KhachHang] ([ID], [tenKhachHang], [SDT], [diaChiKhachHang], [loaiThanhVien]) VALUES (3, N'Nguyễn Phi Khanh                                  ', N'0168163513                                        ', N'91/8 Nguyễn Khoái Q4,TPHCM                        ', 1)
INSERT [dbo].[KhachHang] ([ID], [tenKhachHang], [SDT], [diaChiKhachHang], [loaiThanhVien]) VALUES (4, N'Từ Vĩ Khang                                       ', N'0911561516                                        ', N'91/8 Nguyễn Khoái Q4,TPHCM                        ', 1)
INSERT [dbo].[KhachHang] ([ID], [tenKhachHang], [SDT], [diaChiKhachHang], [loaiThanhVien]) VALUES (5, N'Nguyễn Phi Khanh                                  ', N'0168163513                                        ', N'91/8 Nguyễn Khoái Q4,TPHCM                        ', 1)
SET IDENTITY_INSERT [dbo].[KhachHang] OFF
SET IDENTITY_INSERT [dbo].[SanPham] ON 

INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Asus VivoBook X409FA i3 8145U/4GB/256GB/Win10 (EK468T)', N'Asus', N'Game', 100000, 1)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Asus VivoBook X509JP i5 1035G1/8GB/512GB/2GB MX330/Win10 (EJ023T)', N'Asus', N'Graphics', 1200000, 2)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Asus VivoBook X509FA i3 8145U/4GB/512GB/Chu?t/Win10 (EJ857T)', N'Asus', N'Game', 2000000, 3)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Asus VivoBook A512FA i5 10210U/8GB/512GB/Chu?t/Win10 (EJ1734T)', N'Asus', N'Office', 3000000, 4)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop HP 15s du0058TU N5000/4GB/1TB/Win10 (6ZF55PA)', N'Hp', N'Office', 6520000, 5)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop HP Pavilion 15 cs3012TU i5 1035G1/8GB/512GB/Win10 (8QP30PA)', N'Hp', N'Game', 51651, 6)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Asus VivoBook X509MA N4000/4GB/256GB/Win10 (BR061T)', N'Asus', N'Game', 151020, 7)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo IdeaPad S145 15IIL i3 1005G1/4GB/256GB/Win10 (81W8001XVN)', N'Lenovo', N'Graphics', 653132, 8)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo IdeaPad S145 15IIL i5 1035G1/8GB/512GB/Win10 (81W80021VN)', N'Lenovo', N'Office', 16513210, 9)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo IdeaPad S340 14IIL i3 1005G1/8GB/512GB/Win10 (81VV003VVN)', N'Lenovo', N'Game', 1651310, 10)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo ThinkBook 14IML i5 10210U/8GB/256GB/Win10 (20RV00LVVN)', N'Lenovo', N'Office', 651313, 11)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo ThinkBook 14IML i3 10110U/4GB/256GB/Win10 (20RV00B7VN)', N'Lenovo', N'Game', 16516310, 12)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo IdeaPad S340 15IIL i5 1035G4/8GB/512GB/Win10 (81VW00A8VN)', N'Lenovo', N'Game', 16513510, 13)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo IdeaPad S340 14IIL i5 1035G1/8GB/512...', N'Lenovo', N'Office', 1651310, 14)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo Ideapad S145 15IWL i7 8565U/8GB/512G...', N'Lenovo', N'Game', 165130, 15)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Inspiron 3493 i5 1035G1/8GB/256GB/Win10 (N4I5122WA)', N'Dell', N'Game', 16511110, 16)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Vostro 3590 i7 10510U/8GB/256GB/2GB 610R5/Win10 (GRMGK2)', N'Dell', N'Office', 15500, 17)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Inspiron 5593 i5 1035G1/8GB/256GB/2GB MX230/Win10 (N5I5513W)', N'Dell', N'Game', 150631320, 18)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Vostro 3580 i5 8265U/4GB/1TB/2GB AMD520/Win10 (P75F010V80I)', N'Dell', N'Game', 551531630, 19)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Inspiron 3580 i5 8265U/4GB/1TB/2GB R520/Win10 (70184569)', N'Dell', N'Office', 26516350, 20)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Inspiron 3581 i3 7020U/4GB/1TB/2GB AMD 520/Win10 (N5I3150W)', N'Dell', N'Game', 6516510, 21)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Inspiron 3480 i3 8145U/4GB/1TB/Win10 (NT4X01)', N'Dell', N'Office', 165130, 22)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Vostro 5581 i5 8265U/4GB/1TB/Office365/Win10 (70175950)', N'Dell', N'Game', 16516350, 23)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Dell Vostro 3590 i3 10110U/4GB/1TB/Win10 (V5I3505W)', N'Dell', N'Graphics', 16516350, 24)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Lenovo ThinkBook 14IML i5 10210U/8GB/256GB/Win10 (20RV00LVVN)', N'Lenovo', N'Office', 1651651, 25)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop Asus VivoBook X409JA i3 1005G1/4GB/512GB/Win10 (EK015T)', N'Asus', N'Office', 1000000, 26)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop HP 348 G7 i3 8130U/4GB/256GB/Win10 (9PG83PA)', N'Hp', N'Game', 1000000, 27)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop HP Pavilion 14 ce3027TU i5 1035G1/8GB/16GB+256GB/Win10 (8WJ02PA)', N'Hp', N'Graphics', 100000, 28)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop HP 15s du0063TU i5 8265U/4GB/1TB/Win10 (6ZF63PA)', N'Hp', N'Game', 2230000, 29)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop HP Pavilion 15 cs3119TX i5 1035G1/4GB/256GB/2GB MX250/Win10 (9FN16PA)', N'Hp', N'Office', 6000000, 30)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Laptop HP Pavilion x360 dh0103TU i3 8145U/4GB/1TB/Touch/Win10 (6ZF24PA)', N'Hp', N'Game', 6200000, 31)
INSERT [dbo].[SanPham] ([tenSanPham], [hangSanPham], [dongSanPham], [giaSanPham], [maSanPham]) VALUES (N'Hoang', N'Graphics', N'Asus', 120000, 32)
SET IDENTITY_INSERT [dbo].[SanPham] OFF
SET IDENTITY_INSERT [dbo].[temp] ON 

INSERT [dbo].[temp] ([id], [name]) VALUES (1, N'one       ')
INSERT [dbo].[temp] ([id], [name]) VALUES (2, N'one       ')
INSERT [dbo].[temp] ([id], [name]) VALUES (3, N'one       ')
SET IDENTITY_INSERT [dbo].[temp] OFF
/****** Object:  StoredProcedure [dbo].[procHoaDon]    Script Date: 18/07/2020 4:16:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[procHoaDon]
as
begin
	select *
	from ChiTietHoaDon,HoaDon,KhachHang,SanPham
	where ChiTietHoaDon.maHD=HoaDon.maHD and HoaDon.maKhachHang=KhachHang.ID and ChiTietHoaDon.maSP=SanPham.maSanPham
end
GO
